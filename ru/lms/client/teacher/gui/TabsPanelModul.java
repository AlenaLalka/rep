package ru.lms.client.teacher.gui;

import java.awt.Color;
import java.util.ArrayList;

import ru.lms.client.tools.Teacher;
import ru.lms.modul.Modul;
import ru.lms.modul.ObservableModul;
import ru.lms.modul.ObserverModul;

import com.alee.laf.tabbedpane.WebTabbedPane;

/**
 * Вкладочная панель с модулями.
 */
@SuppressWarnings("serial")
public class TabsPanelModul extends WebTabbedPane implements ObservableModul
{
	/**Список с модулями.*/
	private ArrayList<Modul> modulList;
	/**Активный пользователь*/
	private Teacher activTeacher;

	public TabsPanelModul()
	{
		super(WebTabbedPane.TOP);
		
		this.modulList = new ArrayList<Modul>();
		this.activTeacher = null;
		this.init();
	}
	
	public TabsPanelModul(Teacher teacher)
	{
		//Рассположение вкладок
		super(WebTabbedPane.TOP);
		
		this.modulList = new ArrayList<Modul>();
		this.activTeacher = teacher;
		this.init();
	}
	
	private void init()
	{
		this.setBackground(Color.WHITE);
	}

	@Override
	public void registerObserverModul(Modul modul)
	{
		this.modulList.add(modul);
		
		this.addTab(modul.getNameModul(), modul);
		
		modul.update(this.activTeacher);
		
		int tabcount = this.getTabCount();
		tabcount--;
		this.setSelectedIndex(tabcount);
		
	}

	@Override
	public void removeObserverModul(Modul modul)
	{
		this.modulList.remove(modul);
		this.remove(modul);
	}
	
	@Override
	public void notifyTeacherModul()
	{
		for(ObserverModul modul : modulList)
		{
			modul.update(this.activTeacher);
		}
	}

	public ArrayList<Modul> getModulList()
	{
		return modulList;
	}

	public void setModulList(ArrayList<Modul> modulList)
	{
		this.modulList.clear();
		this.removeAll();
		
		for(Modul modul : modulList)
			this.registerObserverModul(modul);
	}

	public Teacher getActivTeacher()
	{
		return activTeacher;
	}

	public void setActivTeacher(Teacher activTeacher)
	{
		this.activTeacher = activTeacher;
		this.notifyTeacherModul();
	}



}
