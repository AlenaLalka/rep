package ru.lms.client.teacher;



import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.UIManager;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.lms.client.tools.Teacher;
import ru.lms.client.tools.database.xml.DataBaseXmlUsersTeacher;
import ru.lms.lang.Language;
import ru.lms.tools.Resources;

import com.alee.extended.painter.DashedBorderPainter;
import com.alee.laf.WebLookAndFeel;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.text.WebPasswordField;
import com.alee.laf.text.WebTextField;
import com.alee.managers.hotkey.Hotkey;
import com.alee.managers.hotkey.HotkeyManager;
import com.alee.managers.style.skin.web.WebLabelPainter;
import com.alee.utils.SwingUtils;

/**
 * Окно для аутификации преподавателей.
 */
@SuppressWarnings("serial")
public class FrameLoginTeacher extends WebFrame
{
	private static final Logger log = LoggerFactory.getLogger(FrameLoginTeacher.class);	

	/**Список зарегистрированных пользователей преподавателей.*/
	private DataBaseXmlUsersTeacher dbTeacher;
	
	public FrameLoginTeacher()
	{
		this.initBD();
		this.inin();
		
	}
	
	private void initBD()
	{		
		this.dbTeacher = new DataBaseXmlUsersTeacher(Resources.PATH_TEACHER_LIST); 
		this.dbTeacher.openDB();
	}
	
	@SuppressWarnings("rawtypes")
	private void inin()
	{
		this.setSize(451,280);
		this.setDefaultCloseOperation(WebFrame.EXIT_ON_CLOSE);
		
		WebPanel contentPanel = new WebPanel();
		contentPanel.setBackground(UIManager.getColor("CheckBoxMenuItem.background"));
		
		
		/* Обводка лейбла Wekcome */
        WebLabel lblWelkom = new WebLabel("Welcome");
		final DashedBorderPainter bp4 = new DashedBorderPainter ( new float[]{ 3f, 3f } );
        bp4.setRound ( 12 );
        bp4.setWidth ( 2 );
        bp4.setColor ( new Color ( 39, 95, 173 ) );
        lblWelkom.setPainter ( new WebLabelPainter ( bp4 ) ).setMargin ( 10 );
		lblWelkom.setForeground(UIManager.getColor("TitledBorder.titleColor"));
		lblWelkom.setFont(new Font("comic sans ms", Font.PLAIN, 22));
		

		final WebPasswordField passText = new WebPasswordField(10);
		
		final WebTextField loginText = new WebTextField(10);
		
		WebLabel labLog = new WebLabel(Language.LABEL_LOGIN);
		WebLabel labPas = new WebLabel(Language.LABEL_PASSWORD);
		
		 WebButton registerBut = new WebButton ( Language.BUTTTON_REGISTER );
		
	     WebButton enterBut = new WebButton ( Language.BUTTTON_ENTER );
	     
		
		
		GroupLayout groupLayout = new GroupLayout(contentPanel);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblWelkom, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(340, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(156, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(labLog, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(labPas, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(enterBut, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(passText, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(loginText, GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)
						.addComponent(registerBut, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addGap(47))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblWelkom, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 81, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(labLog, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(loginText, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(labPas, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(passText, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(enterBut, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(registerBut, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
					.addGap(40))
		);
		
		
		contentPanel.setLayout(groupLayout);
		
		this.setContentPane(contentPanel);

		this.setLocationRelativeTo(null);
	
      
       
        
        enterBut.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				//Получаем из БД препода. Если его там нет, то будет null.
				final Teacher teacher = dbTeacher.getTeacher(loginText.getText().toLowerCase(), new String(passText.getPassword()));
				
				if(teacher!= null)
				{
					//Открывает окно в новом потоке.
					SwingUtils.invokeLater(new Runnable()
					{
						@Override
						public void run()
						{
							new FrameTeacherClient(teacher).setVisible(true);
							
						}
					});
					
					setVisible(false);
				}
				else
				{
					log.error("Не верный логин или пароль: " + loginText.getText());
					WebOptionPane.showMessageDialog ( getOwner(), "Не верный логин или пароль", "Ошибка", WebOptionPane.ERROR_MESSAGE );
				}
				
			}
		});
        
        registerBut.addActionListener( new ActionListener ()
        {
            @Override
            public void actionPerformed ( ActionEvent e )
            {
            	SwingUtils.invokeLater(new Runnable()
				{
					
					@Override
					public void run()
					{
		                DialogRegisterTeacher frame = new DialogRegisterTeacher(dbTeacher);
		                frame.setVisible(true);
					}
				});

            }
        });

        
        HotkeyManager.registerHotkey( this, enterBut, Hotkey.ENTER );
	}
	
	
	
	
	public static void main(String[] argc)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{   
					WebLookAndFeel.install();

					WebLookAndFeel.setDecorateAllWindows(true);
					
					FrameLoginTeacher frameLog = new FrameLoginTeacher();
					
					frameLog.setVisible(true);
				} 
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}
	
	
	
	/*public void setVisible(boolean visiable) 
	{ this.frameLog.setVisible(visiable); } 
	public boolean isVisible()
	{
		return this.frameLog.isVisible(); 
		}*/
}
