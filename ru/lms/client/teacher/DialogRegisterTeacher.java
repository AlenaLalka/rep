package ru.lms.client.teacher;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;

import ru.lms.client.tools.Teacher;
import ru.lms.client.tools.database.xml.DataBaseXmlUsersTeacher;
import ru.lms.lang.Language;
import ru.lms.tools.Resources;

import com.alee.extended.layout.TableLayout;
import com.alee.extended.panel.CenterPanel;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebDialog;
import com.alee.laf.text.WebPasswordField;
import com.alee.laf.text.WebTextField;
import com.alee.managers.hotkey.Hotkey;
import com.alee.managers.hotkey.HotkeyManager;
import com.alee.utils.SwingUtils;

/**
 * Окно для регистрации нового преподавателя.
 */
@SuppressWarnings("serial")
public class DialogRegisterTeacher extends WebDialog
{
	/**БД для аутификации преподавателей.*/
	private DataBaseXmlUsersTeacher dbTeacher;
	
	/**
	 * @param dbTeacher - БД для аутификации преподавателей.
	 */
	public DialogRegisterTeacher(DataBaseXmlUsersTeacher dbTeacher)
	{
		this.dbTeacher = dbTeacher;
		this.init();
	}
	
	private void init()
	{
		//==================Основная форма======================
		Image img = new ImageIcon(Resources.LOGIN_TEACHER_ICON).getImage();
	    this.setIconImage(img);
	    
		this.setTitle(Language.LABEL_TEACHER_REGISTER_FRAME);
		this.setDefaultCloseOperation ( WebDialog.DISPOSE_ON_CLOSE );
		this.setResizable ( false );
		this.setModal ( true );

        //Сетка 2х5
        TableLayout layout = new TableLayout ( new double[][]{ { TableLayout.PREFERRED, TableLayout.FILL },
                { TableLayout.PREFERRED,
        	TableLayout.PREFERRED,
        	TableLayout.PREFERRED,
        	TableLayout.PREFERRED, 
        	TableLayout.PREFERRED, 
        	TableLayout.PREFERRED,
        	TableLayout.PREFERRED,
        	TableLayout.PREFERRED } } );
        
        //промежуточные расстояния
        layout.setHGap ( 5 );
        layout.setVGap ( 5 );
        
        //Панель со всем контентом
        WebPanel content = new WebPanel ( layout );
        content.setMargin ( 15, 30, 15, 30 );
        content.setOpaque ( false );

        //Текстовые поля
        final WebTextField loginText = new WebTextField ( 15 );
        final WebPasswordField passText = new WebPasswordField ( 15 );
        final WebPasswordField confPassText = new WebPasswordField ( 15 );
        final WebTextField firstNameText = new WebTextField ( 15 );
        final WebTextField secondNameText = new WebTextField ( 15 );
        final WebTextField eMailText = new WebTextField ( 15 );
        final WebTextField workspaceText = new WebTextField ( 15 );
        
        // Координатное добовление: например "0,1" - 0 это столбец, 1- строка
        content.add ( new WebLabel ( Language.LABEL_LOGIN, WebLabel.TRAILING ), "0,0" );
        content.add (loginText, "1,0" );

        content.add ( new WebLabel ( Language.LABEL_PASSWORD, WebLabel.TRAILING ), "0,1" );
        content.add ( passText, "1,1" );
        
        content.add ( new WebLabel ( Language.LABEL_CONFIRM_PASSWORD, WebLabel.TRAILING ), "0,2" );
        content.add (confPassText, "1,2" );

        content.add ( new WebLabel ( Language.LABEL_FIRST_NAME, WebLabel.TRAILING ), "0,3" );
        content.add ( firstNameText, "1,3" );
        
        content.add ( new WebLabel ( Language.LABEL_SECOND_NAME, WebLabel.TRAILING ), "0,4" );
        content.add ( secondNameText, "1,4" );
        
        content.add ( new WebLabel ( Language.LABEL_EMAIL, WebLabel.TRAILING ), "0,5" );
        content.add ( eMailText, "1,5" );
        
        content.add ( new WebLabel ( Language.LABEL_WORKSPACE_PATH, WebLabel.TRAILING ), "0,6" );
        content.add ( workspaceText, "1,6" );

        WebButton registerBut = new WebButton ( Language.BUTTTON_REGISTER );
        WebButton cancelBut = new WebButton ( Language.BUTTTON_CANCEL );
        
        registerBut.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				String pass = new String(passText.getPassword());
				String confPass = new String(confPassText.getPassword());
				
				if (pass.equals(confPass) && !loginText.getText().equals("") && !workspaceText.getText().equals(""))
				{

					Teacher teacher = new Teacher(loginText.getText()
							.toLowerCase(), pass, firstNameText.getText(),
							secondNameText.getText(), eMailText.getText(),
							workspaceText.getText());

					dbTeacher.addTeacherInDB(teacher);
					dbTeacher.saveDB();

					DialogRegisterTeacher.this.dispose();
				}
			}
		});
        
        cancelBut.addActionListener(new ActionListener ()
        {
            @Override
            public void actionPerformed ( ActionEvent e )
            {
            	DialogRegisterTeacher.this.dispose();
            }
        });

        content.add ( new CenterPanel ( new GroupPanel ( 5, registerBut, cancelBut ) ), "0,7,1,7" );
        SwingUtils.equalizeComponentsWidths ( registerBut, cancelBut );

        this.add( content );
        this.pack();
        this.setLocationRelativeTo(null);

        HotkeyManager.registerHotkey( this, cancelBut, Hotkey.ESCAPE );
        HotkeyManager.registerHotkey( this, registerBut, Hotkey.ENTER );
	}
}
