package ru.lms.client.teacher;

import java.awt.EventQueue;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.border.EmptyBorder;
import ru.lms.client.teacher.gui.TabsPanelModul;
import ru.lms.client.tools.Teacher;
import ru.lms.lang.Language;
import ru.lms.modul.ModulCabPanel;

import com.alee.laf.WebLookAndFeel;
import com.alee.laf.menu.MenuBarStyle;
import com.alee.laf.menu.WebMenu;
import com.alee.laf.menu.WebMenuBar;
import com.alee.laf.menu.WebMenuItem;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class FrameTeacherClient extends WebFrame
{
	private static final Logger log = LoggerFactory.getLogger(FrameTeacherClient.class);
	private WebPanel mainPanel;
	private WebMenuBar menuBar;
	private WebMenu fileMenu;
	private TabsPanelModul tabsModuls;
	private Teacher activTeacher;

	public FrameTeacherClient()
	{
		super(Language.TITLE_TEACHER_FRAME);

		this.activTeacher = null;

		this.init();
	}

	public FrameTeacherClient(Teacher teacher)
	{
		super(Language.TITLE_TEACHER_FRAME);

		this.activTeacher = teacher;

		this.init();
	}

	/** Инициализация всех объектов. */
	private void init()
	{

		log.info("Открыто главное окно");
		this.setSize(874, 580);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(WebFrame.EXIT_ON_CLOSE);

		this.mainPanel = new WebPanel();

		/* Меню */
		this.menuBar = new WebMenuBar();
		this.menuBar.setMenuBarStyle(MenuBarStyle.attached);

		fileMenu = new WebMenu(Language.LABEL_FILE);
		WebMenu MenuSp = new WebMenu(Language.LABEL_HELP);
		WebMenu MenuAbout = new WebMenu(Language.LABEL_ABOUT_PROGRAM);

		WebMenuItem exitMenu = new WebMenuItem(Language.LABEL_EXIT);
		WebMenuItem openMenu = new WebMenuItem(Language.LABEL_OPEN);

		fileMenu.add(openMenu);
		fileMenu.add(exitMenu);

		menuBar.add(fileMenu);
		menuBar.add(MenuSp);
		menuBar.add(MenuAbout);

		this.setJMenuBar(menuBar);

		// =======Основная панель=========
		this.mainPanel = new WebPanel();
		// mainPanel.revalidate();
		this.mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.mainPanel.setBackground(Color.WHITE);
		this.setContentPane(this.mainPanel);
		mainPanel.setLayout(new BorderLayout());

		//Специальная вкладочная панель для модулей
		tabsModuls = new TabsPanelModul(this.activTeacher);

		mainPanel.add(tabsModuls, BorderLayout.CENTER);

		// Открываю модули: кабинет, учебный план, таблица
		ModulCabPanel cab = new ModulCabPanel(tabsModuls);
		
		// Добавляю в табпанель кабинет
		tabsModuls.registerObserverModul(cab);
	}

	public static void main(String[] argc)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					WebLookAndFeel.install();

					WebLookAndFeel.setDecorateAllWindows(true);

					new FrameTeacherClient().setVisible(true);

				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

}
