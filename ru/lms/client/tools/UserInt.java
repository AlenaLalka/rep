package ru.lms.client.tools;

/**
 * Интерфейс для пользователей.
 */
public interface UserInt
{
	public String getLogin();
	public String getPassword();
	public void setLogin(String login);
	public void setPassword(String password);
}
