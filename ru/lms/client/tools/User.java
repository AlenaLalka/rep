package ru.lms.client.tools;

/**
 * Абстрактный класс для пользователей.
 */
public abstract class User implements UserInt
{
	/** Логин пользователя. */
	protected String login;
	/** Пароль пользователя. */
	protected String password;

	public User(String login, String pass)
	{
		this.login = login;
		this.password = pass;
	}
	
	public User()
	{
		this(null,null);
	}

	@Override
	public String getLogin()
	{
		return this.login;
	}

	@Override
	public String getPassword()
	{
		return this.password;
	}

	public void setLogin(String login)
	{
		this.login = login;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}
	
	

}
