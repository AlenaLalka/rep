package ru.lms.client.tools;

import java.util.Date;

import ru.lms.client.tools.database.elements.DataBaseElementLaboratory;


public class Laboratory  extends DataBaseElementLaboratory
	{
		protected String nameLab;

		protected String markLab;
		
		protected String maxMark;
		
		protected Date date;

		
		public Laboratory()
		{
			
		}

		public String getNameLab()
		{
			return nameLab;
		}

		public void setNameLab(String namelab)
		{
			this.nameLab = namelab;
		}

		public String getMarkLab()
		{
			return markLab;
		}

		public void setMarkLab(String marklab)
		{
			this.markLab = marklab;
		}

		public String getMaxMark() {
			return maxMark;
		}

		public Date getDate() {
			return date;
		}

		public void setMaxMark(String maxMark) {
			this.maxMark = maxMark;
		}

		public void setDate(Date date) {
			this.date = date;
		}



	}


