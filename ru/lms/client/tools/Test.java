package ru.lms.client.tools;


import ru.lms.client.tools.database.elements.DataBaseElementTest;

public class Test extends DataBaseElementTest
{
	protected String nameTest;
	
	protected String mark;
	
	protected String maxmark;
	
	
	public Test()
	{
		
	}

	public String getNameTest()
	{
		return nameTest;
	}

	public void setNameTest(String nameTest)
	{
		this.nameTest = nameTest;
	}

	public String getMark()
	{
		return mark;
	}

	public void setMark(String mark)
	{
		this.mark = mark;
	}

	public String getMaxmark() {
		return maxmark;
	}

	public void setMaxmark(String maxmark) {
		this.maxmark = maxmark;
	}}
