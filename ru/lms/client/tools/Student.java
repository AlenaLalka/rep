package ru.lms.client.tools;

import java.util.ArrayList;

import ru.lms.client.tools.database.elements.DataBaseElementStudent;

/**
 * Структура для студента.
 */
public class Student extends DataBaseElementStudent
{
	/**Имя.*/
	protected String firstName;
	/**Фамилия.*/
	protected String secondName;
	
	protected ArrayList<Lecture> lectureList;

	protected ArrayList<PracticalClasses> practicalList;
	
	protected ArrayList<Laboratory> labList;

	protected ArrayList<Test> testList;
	
	public Student()
	{
		this.lectureList = new ArrayList<Lecture>();
		this.labList = new ArrayList<Laboratory>();
		this.practicalList = new ArrayList<PracticalClasses>();
		this.testList = new ArrayList<Test>();
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getSecondName()
	{
		return secondName;
	}

	public void setSecondName(String secondName)
	{
		this.secondName = secondName;
	}

	public ArrayList<Lecture> getLectureList()
	{
		return lectureList;
	}

	public ArrayList<Test> getTestList()
	{
		return testList;
	}

	public void setTestList(ArrayList<Test> testList)
	{
		if(testList== null)
			return;
		
		this.testList.clear();
		for(Test ts : testList)
			this.testList.add(ts);
	}
	
	public void setLaboratoryList(ArrayList<Laboratory> labList)
	{
		if(labList== null)
			return;
		
		this.labList.clear();
		for(Laboratory lab : labList)
			this.labList.add(lab);
	}
	
	public ArrayList<Laboratory> getLaboratoryList()
	{
		return labList;
	}

	public ArrayList<PracticalClasses> getPracticalClassesList()
	{
		return practicalList;
	}

	public void setLectureList(ArrayList<Lecture> lectureList)
	{
		if(lectureList== null)
			return;
		
		this.lectureList.clear();
		for(Lecture lec : lectureList)
			this.lectureList.add(lec);
	}
	
	public void setPracticalClassesList(ArrayList<PracticalClasses> practicalList)
	{
		if(practicalList== null)
			return;
		
		this.practicalList.clear();
		for(PracticalClasses prac : practicalList)
			this.practicalList.add(prac);
	}
	
}
