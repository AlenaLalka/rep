package ru.lms.client.tools;

import ru.lms.client.tools.database.elements.DataBaseElementPracticalClasses;

public class PracticalClasses extends DataBaseElementPracticalClasses
{
	protected String namePrac;
	
	protected String visit;
	
	protected int markPrac;
	
	public PracticalClasses()
	{
		
	}

	public String getNamePractical()
	{
		return namePrac;
	}

	public void setNamePractical(String nameprac)
	{
		this.namePrac = nameprac;
	}

	public String getVisit()
	{
		return visit;
	}

	public void setVisit(String visit)
	{
		this.visit = visit;
	}

	public int getMarkPrac() {
		return markPrac;
	}

	public void setMarkPrac(int markPrac) {
		this.markPrac = markPrac;
	}
}

