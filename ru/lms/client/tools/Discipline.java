package ru.lms.client.tools;

import java.util.ArrayList;

import ru.lms.client.tools.database.elements.DataBaseElementDiscipline;

public class Discipline extends DataBaseElementDiscipline
{
	protected String name;
	

	protected ArrayList<Group> groupList;

	public Discipline(String nameDis)
	{
		this.name = nameDis;
		groupList = new ArrayList<Group>();
	}

	public Discipline()
	{
		this("test");
	}

	public void addGroup(Group group)
	{
		this.groupList.add(group);
	}
	
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public ArrayList<Group> getGroupList()
	{
		return groupList;
	}

	
	public void setGroupList(ArrayList<Group> groupList)
	{
		if(groupList!=null)
		{
			this.groupList.clear();
			for(Group group :  groupList)
				this.groupList.add(group);
		}
	}

}
