package ru.lms.client.tools.database;

import java.util.ArrayList;

import ru.lms.client.tools.Discipline;
import ru.lms.client.tools.Group;
import ru.lms.client.tools.Laboratory;
import ru.lms.client.tools.Lecture;
import ru.lms.client.tools.PracticalClasses;
import ru.lms.client.tools.Test;


public interface DataBaseDisciplinesInt extends DataBaseInt
{
	public Discipline addDisciplineToDB(Discipline discipline);
	public ArrayList<Discipline> getAllDiscipline();

	public void addGroupToDisciplineToDB(Group group, String nameDiscipline, ArrayList<Lecture> lectureList ,ArrayList<PracticalClasses> practicalList, ArrayList<Laboratory> laboratoryList,ArrayList<Test> testList);
	
}
