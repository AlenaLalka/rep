package ru.lms.client.tools.database;

import java.util.ArrayList;

import ru.lms.client.tools.Teacher;


/**
 * БД для списка пользователей преподователи.
 */
public interface DataBaseUsersTeacherInt extends DataBaseInt
{
	public Teacher addTeacherInDB(Teacher teacher);
	public Teacher getTeacher(String login, String password);
	public ArrayList<Teacher> getAllTeacher();
}
