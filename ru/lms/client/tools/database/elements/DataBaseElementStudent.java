package ru.lms.client.tools.database.elements;

import ru.lms.client.tools.User;
import ru.lms.client.tools.database.DataBaseGroupInt;

/**
 * Элемент из БД: студент.
 */
public abstract class DataBaseElementStudent extends User implements DataBaseElementInt
{
	protected DataBaseGroupInt dataBase;
	
	public DataBaseElementStudent(DataBaseGroupInt dataBase,String login, String pass)
	{
		super(login, pass);
		
		this.dataBase = dataBase;
	}
	
	public DataBaseElementStudent(DataBaseGroupInt dataBase)
	{
		this(dataBase,null,null);
	}
	
	public DataBaseElementStudent()
	{
		this(null);
	}

	public DataBaseElementStudent(String login, String pass)
	{
		this(null,login,pass);
	}

	@Override
	public void updateDB()
	{

	}
	
	public DataBaseGroupInt getDataBase()
	{
		return dataBase;
	}

	public void setDataBase(DataBaseGroupInt dataBase)
	{
		this.dataBase = dataBase;
	}
}
