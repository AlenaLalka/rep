package ru.lms.client.tools.database.elements;

import ru.lms.client.tools.database.DataBaseDisciplinesInt;

public abstract class DataBaseElementTest implements DataBaseElementInt
{
	protected DataBaseDisciplinesInt dataBase;
	
	public DataBaseElementTest(DataBaseDisciplinesInt dataBase)
	{
		this.dataBase = dataBase;
	}
	
	@Override
	public void updateDB()
	{

	}
	
	public DataBaseElementTest()
	{
		this(null);
	}

	public DataBaseDisciplinesInt getDataBase()
	{
		return dataBase;
	}

	public void setDataBase(DataBaseDisciplinesInt dataBase)
	{
		this.dataBase = dataBase;
	}

}
