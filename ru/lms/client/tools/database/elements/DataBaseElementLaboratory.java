package ru.lms.client.tools.database.elements;

import ru.lms.client.tools.database.DataBaseDisciplinesInt;

public abstract class DataBaseElementLaboratory implements DataBaseElementInt
{
	protected DataBaseDisciplinesInt dataBase;
	
	public DataBaseElementLaboratory(DataBaseDisciplinesInt dataBase)
	{
		this.dataBase = dataBase;
	}
	
	@Override
	public void updateDB()
	{

	}
	
	public DataBaseElementLaboratory()
	{
		this(null);
	}

	public DataBaseDisciplinesInt getDataBase()
	{
		return dataBase;
	}

	public void setDataBase(DataBaseDisciplinesInt dataBase)
	{
		this.dataBase = dataBase;
	}

}