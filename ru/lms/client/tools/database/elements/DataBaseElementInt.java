package ru.lms.client.tools.database.elements;

/**
 * Интерфейс для элемента из баз данных.
 *
 */
public interface DataBaseElementInt
{
	/**Обновляет содержимое элемента в базе данных.*/
	public void updateDB();
}
