package ru.lms.client.tools.database.elements;

import ru.lms.client.tools.database.DataBaseGroupInt;


/**
 * Элемент из БД: группа.
 */
public abstract class DataBaseElementGroup implements DataBaseElementInt
{
	protected DataBaseGroupInt dataBase;
	
	public DataBaseElementGroup(DataBaseGroupInt dataBase)
	{
		this.dataBase = dataBase;
	}
	
	public DataBaseElementGroup()
	{
		this(null);
	}

	@Override
	public void updateDB()
	{
		// TODO Auto-generated method stub
		
	}
	
	public DataBaseGroupInt getDataBase()
	{
		return dataBase;
	}

	public void setDataBase(DataBaseGroupInt dataBase)
	{
		this.dataBase = dataBase;
	}
}
