package ru.lms.client.tools.database.elements.xml;

import org.w3c.dom.Element;

import ru.lms.client.tools.ConstantsTools;
import ru.lms.client.tools.Lecture;
import ru.lms.client.tools.database.DataBaseDisciplinesInt;

public class DataBaseXmlElementLecture extends Lecture
{
	private Element xmlElement;
	
	public DataBaseXmlElementLecture(Element xmlElement, DataBaseDisciplinesInt dataBase)
	{
		this.dataBase = dataBase;
		this.setXmlElement(xmlElement);
		
	}

	public Element getXmlElement()
	{
		return xmlElement;
	}

	public void setXmlElement(Element xmlElement)
	{
		this.xmlElement = xmlElement;
		
		this.nameLecture = this.xmlElement.getAttribute(ConstantsTools.LECTURE_ELEMENT_NAME);
		this.visit = this.xmlElement.getAttribute(ConstantsTools.LECTURE_VISIT_ATTRIBUTE);
	}
}
