package ru.lms.client.tools.database.elements.xml;

import org.w3c.dom.Element;

import ru.lms.client.tools.ConstantsTools;
import ru.lms.client.tools.Test;
import ru.lms.client.tools.database.DataBaseDisciplinesInt;

public class DataBaseXmlElementTest extends Test
	{
		private Element xmlElement;
		
		public DataBaseXmlElementTest(Element xmlElement, DataBaseDisciplinesInt dataBase)
		{
			this.dataBase = dataBase;
			this.setXmlElement(xmlElement);
			
		}

		public Element getXmlElement()
		{
			return xmlElement;
		}

		public void setXmlElement(Element xmlElement)
		{
			this.xmlElement = xmlElement;
			
			this.nameTest = this.xmlElement.getAttribute(ConstantsTools.TEST_ELEMENT_NAME);
			this.mark =this.xmlElement.getAttribute(ConstantsTools.MARK_ATTRIBUTE);
		
		}
}
