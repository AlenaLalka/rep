package ru.lms.client.tools.database.elements.xml;

import org.w3c.dom.Element;

import ru.lms.client.tools.ConstantsTools;
import ru.lms.client.tools.Laboratory;
import ru.lms.client.tools.Lecture;
import ru.lms.client.tools.PracticalClasses;
import ru.lms.client.tools.Student;
import ru.lms.client.tools.Test;
import ru.lms.client.tools.database.DataBaseGroupInt;


public class DataBaseXmlElementStudent extends Student
{
	private Element xmlElement;
	
	public DataBaseXmlElementStudent(Element xmlElement, DataBaseGroupInt dataBase)
	{
		this.dataBase = dataBase;
		this.setXmlElement(xmlElement);
	}

	public Element getXmlElement()
	{
		return xmlElement;
	}

	public void setXmlElement(Element xmlElement)
	{
		this.xmlElement = xmlElement;
		
		this.login = this.xmlElement.getAttribute(ConstantsTools.ACCOUNT_LOGIN_ATTRIBUTE);
		this.password = this.xmlElement.getAttribute(ConstantsTools.ACCOUNT_PASSWORD_ATTRIBUTE);
		this.firstName = this.xmlElement.getAttribute(ConstantsTools.STUDENT_FIRST_NAME_ATTRIBUTE);
		this.secondName = this.xmlElement.getAttribute(ConstantsTools.STUDENT_SECOND_NAME_ATTRIBUTE);	
	}

	public void updateDB()
	{
		for(Lecture lec : lectureList)
		{
			DataBaseXmlElementLecture lecXml = (DataBaseXmlElementLecture) lec;
			this.xmlElement.appendChild(lecXml.getXmlElement());
		}
		
		for(PracticalClasses pr : practicalList)
		{
			DataBaseXmlElementPracticalClasses prXml = (DataBaseXmlElementPracticalClasses) pr;
			this.xmlElement.appendChild(prXml.getXmlElement());
		}
		
		for(Laboratory lb : labList)
		{
			DataBaseXmlElementLaboratory labXml = (DataBaseXmlElementLaboratory) lb;
			this.xmlElement.appendChild(labXml.getXmlElement());
		}
		
		for(Test ts : testList)
		{
			DataBaseXmlElementTest tsXml = (DataBaseXmlElementTest) ts;
			this.xmlElement.appendChild(tsXml.getXmlElement());
		}
	}
}
