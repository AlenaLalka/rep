package ru.lms.client.tools.database.elements.xml;

import org.w3c.dom.Element;

import ru.lms.client.tools.ConstantsTools;
import ru.lms.client.tools.Group;
import ru.lms.client.tools.database.DataBaseGroupInt;


public class DataBaseXmlElementGroup extends Group
{
	private Element xmlElement;
	
	public DataBaseXmlElementGroup(Element xmlElement, DataBaseGroupInt dataBase)
	{
		this.dataBase = dataBase;
		this.setXmlElement(xmlElement);
	}

	public Element getXmlElement()
	{
		return xmlElement;
	}

	public void setXmlElement(Element xmlElement)
	{
		this.xmlElement = xmlElement;
		
		this.nameGroup = this.xmlElement.getAttribute(ConstantsTools.GROUP_ELEMENT_NAME);	
	}
	
	@Override
	public void updateDB()
	{
		this.xmlElement.setAttribute(ConstantsTools.GROUP_ELEMENT_NAME, this.nameGroup);
		
		
	}
}
