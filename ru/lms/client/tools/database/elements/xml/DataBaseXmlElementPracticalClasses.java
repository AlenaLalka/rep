package ru.lms.client.tools.database.elements.xml;

import org.w3c.dom.Element;

import ru.lms.client.tools.ConstantsTools;
import ru.lms.client.tools.PracticalClasses;
import ru.lms.client.tools.database.DataBaseDisciplinesInt;

public class DataBaseXmlElementPracticalClasses extends PracticalClasses
{
	private Element xmlElement;
	
	public DataBaseXmlElementPracticalClasses(Element xmlElement, DataBaseDisciplinesInt dataBase)
	{
		this.dataBase = dataBase;
		this.setXmlElement(xmlElement);
		
	}

	public Element getXmlElement()
	{
		return xmlElement;
	}

	public void setXmlElement(Element xmlElement)
	{
		this.xmlElement = xmlElement;
		
		this.namePrac = this.xmlElement.getAttribute(ConstantsTools.PRAC_ELEMENT_NAME);
		this.visit = this.xmlElement.getAttribute(ConstantsTools.PRAC_VISIT_ATTRIBUTE);
	}
}



