package ru.lms.client.tools.database.elements.xml;

import org.w3c.dom.Element;

import ru.lms.client.tools.ConstantsTools;
import ru.lms.client.tools.Teacher;
import ru.lms.client.tools.database.DataBaseUsersTeacherInt;

/**
 * Структура преподаватель для БД-xml.
 */
public class DataBaseXmlElementTeacher extends Teacher
{
	/** Объект в БД xml. */
	private Element xmlElement;

	public DataBaseXmlElementTeacher(Element xmlElement,DataBaseUsersTeacherInt dataBase)
	{
		this.setXmlElement(xmlElement);
		this.dataBaseTeachers = dataBase;
	}

	public void setXmlElement(Element teacherElement)
	{
		this.xmlElement = teacherElement;
		
		this.firstName = teacherElement.getAttribute(ConstantsTools.TEACHER_FIRST_NAME_ATTRIBUTE);
		this.secondName = teacherElement.getAttribute(ConstantsTools.TEACHER_SECOND_NAME_ATTRIBUTE);
		this.eMail = teacherElement.getAttribute(ConstantsTools.TEACHER_EMAIL_ATTRIBUTE);
		this.workspacePath = teacherElement.getAttribute(ConstantsTools.TEACHER_WORKSPACE_PATH_ATTRIBUTE);
		this.login = teacherElement.getAttribute(ConstantsTools.ACCOUNT_LOGIN_ATTRIBUTE);
		this.password = teacherElement.getAttribute(ConstantsTools.ACCOUNT_PASSWORD_ATTRIBUTE);
	}
	
	public Element getXmlElement()
	{
		return this.xmlElement;
	}	

	@Override
	public void updateDB()
	{
		xmlElement.setAttribute(ConstantsTools.TEACHER_EMAIL_ATTRIBUTE, this.eMail);
		xmlElement.setAttribute(ConstantsTools.TEACHER_SECOND_NAME_ATTRIBUTE, this.secondName);
		xmlElement.setAttribute(ConstantsTools.TEACHER_FIRST_NAME_ATTRIBUTE, this.firstName);
		xmlElement.setAttribute(ConstantsTools.TEACHER_WORKSPACE_PATH_ATTRIBUTE, this.workspacePath);
		xmlElement.setAttribute(ConstantsTools.ACCOUNT_LOGIN_ATTRIBUTE, this.login);
		xmlElement.setAttribute(ConstantsTools.ACCOUNT_PASSWORD_ATTRIBUTE, this.password);
	}

}
