package ru.lms.client.tools.database.elements.xml;

import org.w3c.dom.Element;

import ru.lms.client.tools.ConstantsTools;
import ru.lms.client.tools.Laboratory;
import ru.lms.client.tools.database.DataBaseDisciplinesInt;

public class DataBaseXmlElementLaboratory extends Laboratory
	{
		private Element xmlElement;
		
		public DataBaseXmlElementLaboratory(Element xmlElement, DataBaseDisciplinesInt dataBase)
		{
			this.dataBase = dataBase;
			this.setXmlElement(xmlElement);
			
		}

		public Element getXmlElement()
		{
			return xmlElement;
		}

		public void setXmlElement(Element xmlElement)
		{
			this.xmlElement = xmlElement;
			
			this.nameLab = this.xmlElement.getAttribute(ConstantsTools.LAB_ELEMENT_NAME);
	        this.markLab =this.xmlElement.getAttribute(ConstantsTools.MARK_ATTRIBUTE);
			
		}

}
