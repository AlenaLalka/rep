package ru.lms.client.tools.database.elements.xml;

import org.w3c.dom.Element;
import ru.lms.client.tools.ConstantsTools;
import ru.lms.client.tools.Discipline;
import ru.lms.client.tools.Group;
import ru.lms.client.tools.database.DataBaseDisciplinesInt;

public class DataBaseXmlElementDiscipline extends Discipline
{

	private Element xmlElement;

	public DataBaseXmlElementDiscipline(Element disciplineElement,DataBaseDisciplinesInt dataBaseXmlDiscipline)
	{
		this.xmlElement = disciplineElement;

		this.dataBase = dataBaseXmlDiscipline;

		this.setXmlNameElement(xmlElement);
	}
	

	public void setXmlNameElement(Element disElement)
	{
		this.name = disElement.getAttribute(ConstantsTools.DISCIPLINE_ELEMENT_NAME);
	
	}

	public Element getXmlElement()
	{
		return this.xmlElement;
	}

	@Override
	public void updateDB()
	{
		xmlElement.setAttribute(ConstantsTools.DISCIPLINE_ELEMENT_NAME,this.name);
		
		for (Group group : this.groupList)
		{
			DataBaseXmlElementGroup xmlGroup= (DataBaseXmlElementGroup) group;
			xmlElement.appendChild(xmlGroup.getXmlElement());
		}
	}

}
