package ru.lms.client.tools.database.elements;

import ru.lms.client.tools.database.DataBaseDisciplinesInt;


public abstract class DataBaseElementPracticalClasses implements DataBaseElementInt

	{
		protected DataBaseDisciplinesInt dataBase;
		
		public DataBaseElementPracticalClasses(DataBaseDisciplinesInt dataBase)
		{
			this.dataBase = dataBase;
		}
		
		@Override
		public void updateDB()
		{

		}
		
		public DataBaseElementPracticalClasses()
		{
			this(null);
		}

		public DataBaseDisciplinesInt getDataBase()
		{
			return dataBase;
		}

		public void setDataBase(DataBaseDisciplinesInt dataBase)
		{
			this.dataBase = dataBase;
		}

}
