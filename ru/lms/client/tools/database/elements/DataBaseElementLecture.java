package ru.lms.client.tools.database.elements;

import ru.lms.client.tools.database.DataBaseDisciplinesInt;


public abstract class DataBaseElementLecture implements DataBaseElementInt
{
	protected DataBaseDisciplinesInt dataBase;
	
	public DataBaseElementLecture(DataBaseDisciplinesInt dataBase)
	{
		this.dataBase = dataBase;
	}
	
	@Override
	public void updateDB()
	{

	}
	
	public DataBaseElementLecture()
	{
		this(null);
	}

	public DataBaseDisciplinesInt getDataBase()
	{
		return dataBase;
	}

	public void setDataBase(DataBaseDisciplinesInt dataBase)
	{
		this.dataBase = dataBase;
	}

}
