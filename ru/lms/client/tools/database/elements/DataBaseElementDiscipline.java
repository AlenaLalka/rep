package ru.lms.client.tools.database.elements;

import ru.lms.client.tools.database.DataBaseDisciplinesInt;

/**
 * Элемент из БД дисциплина.
 */
public abstract class DataBaseElementDiscipline implements DataBaseElementInt
{
	protected DataBaseDisciplinesInt dataBase;
	
	public DataBaseElementDiscipline(DataBaseDisciplinesInt dataBase)
	{
		this.dataBase = dataBase;
	}
	
	public DataBaseElementDiscipline()
	{
		this(null);
	}
	
	@Override
	public void updateDB()
	{
	}
	
	public DataBaseDisciplinesInt getDataBase() {
		return dataBase;
	}

	public void setDataBase(DataBaseDisciplinesInt dataBase) {
		this.dataBase = dataBase;
	}
}
