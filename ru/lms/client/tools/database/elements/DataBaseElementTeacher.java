package ru.lms.client.tools.database.elements;

import ru.lms.client.tools.User;
import ru.lms.client.tools.database.DataBaseDisciplinesInt;
import ru.lms.client.tools.database.DataBaseGroupInt;
import ru.lms.client.tools.database.DataBaseUsersTeacherInt;

/**
 * Элемент из БД: преподаватель.
 */
public abstract class DataBaseElementTeacher extends User implements DataBaseElementInt
{
	protected DataBaseUsersTeacherInt dataBaseTeachers;
	
	protected DataBaseDisciplinesInt dataBaseDisciplines;
	
	protected DataBaseGroupInt dataBaseStudents;

	public DataBaseElementTeacher(String login, String pass)
	{
		this(null,login,pass);
	}
	
	public DataBaseElementTeacher(DataBaseUsersTeacherInt dataBase,String login, String pass)
	{
		super(login,pass);
		
		this.dataBaseTeachers = dataBase;
	}
	
	public DataBaseElementTeacher(DataBaseUsersTeacherInt dataBase)
	{
		this(dataBase,null,null);
	}
	
	public DataBaseElementTeacher()
	{
		this(null);
	}

	@Override
	public void updateDB()
	{

	}
	
	public DataBaseUsersTeacherInt getDataBaseTeachers()
	{
		return dataBaseTeachers;
	}

	public void setDataBaseTeachers(DataBaseUsersTeacherInt dataBaseTeachers)
	{
		this.dataBaseTeachers = dataBaseTeachers;
	}

	public DataBaseDisciplinesInt getDataBaseDisciplines()
	{
		return dataBaseDisciplines;
	}

	public void setDataBaseDisciplines(DataBaseDisciplinesInt dataBaseDisciplines)
	{
		this.dataBaseDisciplines = dataBaseDisciplines;
	}

	public DataBaseGroupInt getDataBaseStudents()
	{
		return dataBaseStudents;
	}

	public void setDataBaseStudents(DataBaseGroupInt dataBaseStudents)
	{
		this.dataBaseStudents = dataBaseStudents;
	}
	
	
	
}
