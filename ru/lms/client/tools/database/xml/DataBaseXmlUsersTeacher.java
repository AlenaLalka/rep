package ru.lms.client.tools.database.xml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import ru.lms.client.tools.ConstantsTools;
import ru.lms.client.tools.Discipline;
import ru.lms.client.tools.Group;
import ru.lms.client.tools.Teacher;
import ru.lms.client.tools.database.DataBaseUsersTeacherInt;
import ru.lms.client.tools.database.elements.xml.DataBaseXmlElementTeacher;
import ru.lms.tools.Crypt;
import ru.lms.tools.Resources;
/**
 * База данных для аутификации преподавателей.
 */
public class DataBaseXmlUsersTeacher extends DataBaseXml implements DataBaseUsersTeacherInt
{

	private static final Logger log = LoggerFactory.getLogger(DataBaseXmlUsersTeacher.class);	
	
	private ArrayList<Teacher> teachList;
	
	/**
	 * @param pathXml - путь к xml файлу.
	 */
	public DataBaseXmlUsersTeacher(String pathXml)
	{
		super(pathXml+"\\TeacherList.xml");
	}

	/**
	 * Добавить пореподавателя в xml базу.
	 * @param teacher
	 */
	public Teacher addTeacherInDB(Teacher teacher)
	{
		log.info("Добавление нового пользователя...");
		Element rootElem = this.xmlDataBase.getDocumentElement();

		teacher.setPassword(Crypt.getStringToMD5(teacher.getPassword()));
		
		Element teach = this.xmlDataBase.createElement(ConstantsTools.TEACHER_ELEMENT);	
		teach.setAttribute(ConstantsTools.TEACHER_EMAIL_ATTRIBUTE, teacher.geteMail());
		teach.setAttribute(ConstantsTools.TEACHER_SECOND_NAME_ATTRIBUTE, teacher.getSecondName());
		teach.setAttribute(ConstantsTools.TEACHER_FIRST_NAME_ATTRIBUTE, teacher.getFirstName());
		teach.setAttribute(ConstantsTools.TEACHER_WORKSPACE_PATH_ATTRIBUTE, Resources.PATH_WORKSPACE + teacher.getWorkspacePath());
		teach.setAttribute(ConstantsTools.ACCOUNT_LOGIN_ATTRIBUTE, teacher.getLogin());
		teach.setAttribute(ConstantsTools.ACCOUNT_PASSWORD_ATTRIBUTE, teacher.getPassword());


		rootElem.appendChild(teach);
		
		DataBaseXmlElementTeacher retTeach = new DataBaseXmlElementTeacher(teach,this);
		
		new File(retTeach.getWorkspacePath()).mkdirs();
		
		DataBaseXmlDiscipline disDB = new DataBaseXmlDiscipline(retTeach.getWorkspacePath());
		DataBaseXmlGroup studDB = new DataBaseXmlGroup(retTeach.getWorkspacePath());
		disDB.openDB();
		studDB.openDB();
		
		retTeach.setDataBaseDisciplines(disDB);
		retTeach.setDataBaseStudents(studDB);
		
		this.teachList.add(retTeach);
		
		return retTeach;
		
	}

	@Override
	public void openDB()
	{
		if(!this.xmlFile.exists())
		{
			new File(this.pathXmlFile.substring(0, this.pathXmlFile.length()-"TeacherList.xml".length())).mkdirs();
			
			PrintWriter writer = null;
			try
			{
				writer = new PrintWriter(this.pathXmlFile, "UTF-8");
				writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
				writer.println("<" + ConstantsTools.TEACHER_ELEMENT + "s>");
				writer.println("</" +ConstantsTools.TEACHER_ELEMENT+"s>");
				writer.close();
			} 
			catch (FileNotFoundException | UnsupportedEncodingException e)
			{
				log.error(e.getMessage());
				e.printStackTrace();
			}
		}
		
		super.openDB();
		
		this.readTeachers();
		
	}
	
	/**Считать всех пользователей.*/
	private void readTeachers()
	{
		log.info("Считывание всех пользователей.");

		this.teachList = new ArrayList<Teacher>();

		NodeList teacherList = this.xmlDataBase.getElementsByTagName(ConstantsTools.TEACHER_ELEMENT);

		for (int je = 0; je < teacherList.getLength(); je++)
		{
			Node teacherNode = teacherList.item(je);

			if (teacherNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element teacherElement = (Element) teacherNode;

				DataBaseXmlElementTeacher retTeach = new DataBaseXmlElementTeacher(
						teacherElement, this);

				new File(retTeach.getWorkspacePath()).mkdirs();

				DataBaseXmlDiscipline disDB = new DataBaseXmlDiscipline(retTeach.getWorkspacePath());
				DataBaseXmlGroup groupDB = new DataBaseXmlGroup(retTeach.getWorkspacePath());

				retTeach.setDataBaseDisciplines(disDB);
				retTeach.setDataBaseStudents(groupDB);

				disDB.openDB();
				groupDB.openDB();

				//Получаем все дисциплины
				for (Discipline dic : disDB.getAllDiscipline())
				{				
					//Массив групп со студентами для определенной дисциплины
					ArrayList<Group> groupWithStudList = new ArrayList<Group>();
					//Считывает из БД группы со студентами
					for (Group groupWithStud : groupDB.getGroups())
					{
						//Считывает все группы вкл. в дисциплину
						for (Group group : dic.getGroupList())
						{
							if(groupWithStud.getNameGroup().equals(group.getNameGroup()))
							{
								//Добовляем лекции
								groupWithStud.setLectureList(group.getLectureList());

								//Добовляем практики
								groupWithStud.setPracticalClassesList(group.getPracticalClassesList());
								
								groupWithStud.setLaboratoryList(group.getLaboratoryList());
								
								groupWithStud.setTestList(group.getTestList());

								//Добовляем группу со студентами в массив для дисциплины
								groupWithStudList.add(groupWithStud);
								
							}
						}
					}
					if (groupWithStudList.size()!=0)
					dic.setGroupList(groupWithStudList);
				}
				
				retTeach.setDisciplineList(disDB.getAllDiscipline());

				teachList.add(retTeach);

			}
		}

	}
	
	/**
	 * Получить по логину и паролю преподавателя.
	 * @param login
	 * @param pass
	 * @return
	 */
	public Teacher getTeacher(String login, String pass)
	{
		DataBaseXmlElementTeacher teach = null;
		//Пароль в MD5
		String cryptPass = Crypt.getStringToMD5(pass);
		//Логин в нижний регистр.
		String lowCaseLogin = login.toLowerCase();
		
		for(Teacher teacher : this.teachList)
		{			
			if(teacher.getLogin().equals(lowCaseLogin) && teacher.getPassword().equals(cryptPass))
			{
				return teacher;
			}
		}
		
		return teach;
	}
	
	/**
	 * Получить всех зарегистрированных преподавателей.
	 * @return
	 */
	public ArrayList<Teacher> getAllTeacher()
	{		
		return this.teachList;
	}
	
	

}
