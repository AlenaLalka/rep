package ru.lms.client.tools.database.xml;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import ru.lms.client.tools.database.DataBaseInt;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

/**
 * База данных из xml-файла.
 */
public class DataBaseXml implements DataBaseInt
{
	private static final Logger log = LoggerFactory.getLogger(DataBaseXml.class);	
	
	/**Путь к xml файлу.*/
	protected String pathXmlFile;
	/**Xml файл.*/
	protected File xmlFile;
	/**База данных со всеми данными.*/
	protected Document xmlDataBase;
	
	public DataBaseXml(String pathXml)
	{
		this.pathXmlFile = pathXml;
		
		this.xmlFile = new File(pathXml);
       
	}

	@Override
	public void openDB()
	{
        try
        {
        	//Фабрика билдеров док-тов.
        	DocumentBuilderFactory documentBuilderFactory =DocumentBuilderFactory.newInstance();
        	//Билдер док-в.
        	DocumentBuilder db=documentBuilderFactory.newDocumentBuilder();
        	//Считывает xml док-т.
            this.xmlDataBase=db.parse(this.xmlFile);
            
            log.info("Открыта БД:" + this.xmlFile);
        }
        catch(Exception ei)
        {
        	log.error("Не удалось открыть БД: " + this.xmlFile);
        }
	}

	@Override
	public void closeDB()
	{
		log.info("Закрыта БД: " + this.xmlFile);
	}

	@Override
	public void saveDB()
	{
		//форматируем xml док-т
		OutputFormat format = new OutputFormat(this.xmlDataBase);
        format.setLineWidth(65);
        format.setIndenting(true);
        format.setIndent(2);
        
		XMLSerializer serializer = new XMLSerializer(format);

	    try
		{
			serializer.setOutputCharStream(new java.io.FileWriter(this.xmlFile));
			serializer.serialize(this.xmlDataBase);
		} 
	    catch (IOException e)
		{
	    	log.error("Не удалось сериализовать xml документ: " + this.xmlFile);
		}
	    log.info("Сохранение БД: " + this.xmlFile + " завернено.");
	}

	@Override
	public void updateDB()
	{
		this.closeDB();
		this.openDB();
	}
}
