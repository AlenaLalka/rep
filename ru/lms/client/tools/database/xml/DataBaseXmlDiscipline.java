package ru.lms.client.tools.database.xml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import ru.lms.client.tools.ConstantsTools;
import ru.lms.client.tools.Discipline;
import ru.lms.client.tools.Group;
import ru.lms.client.tools.Laboratory;
import ru.lms.client.tools.Lecture;
import ru.lms.client.tools.PracticalClasses;
import ru.lms.client.tools.Test;
import ru.lms.client.tools.database.DataBaseDisciplinesInt;
import ru.lms.client.tools.database.elements.xml.DataBaseXmlElementDiscipline;
import ru.lms.client.tools.database.elements.xml.DataBaseXmlElementGroup;
import ru.lms.client.tools.database.elements.xml.DataBaseXmlElementLaboratory;
import ru.lms.client.tools.database.elements.xml.DataBaseXmlElementLecture;
import ru.lms.client.tools.database.elements.xml.DataBaseXmlElementPracticalClasses;
import ru.lms.client.tools.database.elements.xml.DataBaseXmlElementTeacher;
import ru.lms.client.tools.database.elements.xml.DataBaseXmlElementTest;
import ru.lms.tools.Crypt;
import ru.lms.tools.Resources;

public class DataBaseXmlDiscipline extends DataBaseXml implements DataBaseDisciplinesInt
{
	private static final Logger log = LoggerFactory.getLogger(DataBaseXmlDiscipline.class);
	
	private ArrayList<Discipline> disList;
	
	/**
	 * @param pathXml
	 *            - путь к xml файлу.
	 */
	public DataBaseXmlDiscipline(String pathXml)
	{
		super(pathXml+"\\DisciplineList.xml");
	}

	/**
	 * Добавить дисциплину в xml базу.
	 */
	public Discipline addDisciplineToDB(Discipline dis)
	{
		log.info("Добавление новой дисциплины...");
		Element rootElem = this.xmlDataBase.getDocumentElement();

		Element Dis = this.xmlDataBase.createElement("Discipline");
		Dis.setAttribute("name", dis.getName());
		
		rootElem.appendChild(Dis);
		
		DataBaseXmlElementDiscipline disXml = new DataBaseXmlElementDiscipline(Dis, this);
		
		return disXml;

	}
	
	public void addGroupToDisciplineToDB(Group group, String nameDiscipline, ArrayList<Lecture> lectureList ,ArrayList<PracticalClasses> practicalList, ArrayList<Laboratory> laboratoryList,ArrayList<Test> testList)
	{
		log.info("Добавление новой группы, плана...");
		
		Element groupElement= null;
		Element rootElem = this.xmlDataBase.getDocumentElement();
		
		NodeList DisList = this.xmlDataBase.getElementsByTagName(ConstantsTools.DISCIPLINE_ELEMENT);
		
		for (int je = 0; je < DisList.getLength(); je++)
		{
			Node discipNode = DisList.item(je);

			if (discipNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element disciplineElement = (Element) discipNode;

				if (disciplineElement.getAttribute("name").equals(nameDiscipline))
					
				{	
					
		        groupElement = this.xmlDataBase.createElement("group");	
	            groupElement.setAttribute("name", group.getNameGroup());
	            
	            
	         
	      
	            
	            for (Lecture lec :  group.getLectureList())
	            {
	            Element lecElement = this.xmlDataBase.createElement("lecture");
	            lecElement.setAttribute("name", lec.getNameLecture());
	            groupElement.appendChild(lecElement);
	            }
	            
	            for(PracticalClasses prac : group.getPracticalClassesList())
	            {
	            Element prElement = this.xmlDataBase.createElement("practical");
	            prElement.setAttribute("name", prac.getNamePractical());
	            groupElement.appendChild(prElement);
	            }
	            for(Laboratory lab :  group.getLaboratoryList())
	            {
	            Element labElement = this.xmlDataBase.createElement("laboratory");
	            labElement.setAttribute("name", lab.getNameLab());
	            labElement.setAttribute("maxMark", lab.getMaxMark());
	            groupElement.appendChild(labElement);
	            }
	            for(Test test : group.getTestList())
	            {
	            Element testElement = this.xmlDataBase.createElement("test");
	            testElement.setAttribute("name", test.getNameTest());
	            testElement.setAttribute("maxMark", test.getMaxmark());
	            groupElement.appendChild(testElement);
	            }
	            
	            disciplineElement.appendChild(groupElement);
		        
		        
				}
				
			}
		}
		
		DataBaseXmlElementGroup groupXml = new DataBaseXmlElementGroup(groupElement, null);
		
		
	}

	
	@Override
	public void openDB()
	{
		if(!this.xmlFile.exists())
		{			
			PrintWriter writer = null;
			try
			{
				writer = new PrintWriter(this.pathXmlFile, "UTF-8");
				writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
				writer.println("<" + ConstantsTools.DISCIPLINE_ELEMENT + "s>");
				writer.println("</" +ConstantsTools.DISCIPLINE_ELEMENT+"s>");
				writer.close();
			} 
			catch (FileNotFoundException | UnsupportedEncodingException e)
			{
				log.error(e.getMessage());
				e.printStackTrace();
			}
		}
		
		
		super.openDB();
		
		this.readDic();
	}
	
	private void readDic()
	{
		disList = new ArrayList<Discipline>();

		NodeList DisList = this.xmlDataBase.getElementsByTagName(ConstantsTools.DISCIPLINE_ELEMENT);

		for (int je = 0; je < DisList.getLength(); je++)
		{
			Node discipNode = DisList.item(je);

			if (discipNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element disciplineElement = (Element) discipNode;

				DataBaseXmlElementDiscipline disXml = new DataBaseXmlElementDiscipline(disciplineElement,this);

	
				disXml.setGroupList(this.getGroups(disciplineElement));
				
				disList.add(disXml);
			}
		}
	}
	
	private ArrayList<Group> getGroups(Element disciplineElement)
	{
		ArrayList<Group> groupList = new ArrayList<Group>();
		
		NodeList groupNodeList = disciplineElement.getElementsByTagName(ConstantsTools.GROUP_ELEMENT);

		for (int groupID = 0; groupID < groupNodeList.getLength(); groupID++)
		{
			Node groupNode = groupNodeList.item(groupID);

			if (groupNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element groupElement = (Element) groupNode;
				
				Group group = new Group();
				
				group.setNameGroup(groupElement.getAttribute(ConstantsTools.GROUP_ELEMENT_NAME));
				
				group.setLectureList(this.getLectures(groupElement));
				group.setPracticalClassesList(this.getPracticalClasses(groupElement));
				group.setLaboratoryList(this.getLaboratory(groupElement));
				group.setTestList(this.getTest(groupElement));
				
				groupList.add(group);
			}
		}
		
		return groupList;
	}

	private ArrayList<Lecture> getLectures(Element groupElement)
	{
		ArrayList<Lecture> lectureList = new ArrayList<Lecture>();
		
		NodeList lecNodeList = groupElement.getElementsByTagName(ConstantsTools.LECTURE_ELEMENT);

		for (int lecID = 0; lecID < lecNodeList.getLength(); lecID++)
		{
			Node lecNode = lecNodeList.item(lecID);

			if (lecNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element lecElement = (Element) lecNode;
				
				DataBaseXmlElementLecture lecture = new DataBaseXmlElementLecture(lecElement, null);
				
				lecture.setNameLecture(lecElement.getAttribute(ConstantsTools.LECTURE_ELEMENT_NAME));
				
				
				
				
				lectureList.add(lecture);
			}
		}
		
		return lectureList;
	}
	
	@SuppressWarnings("unused")
	private ArrayList<PracticalClasses> getPracticalClasses(Element groupElement)
	{
		ArrayList<PracticalClasses> pracList = new ArrayList<PracticalClasses>();
		
		NodeList pracNodeList = groupElement.getElementsByTagName(ConstantsTools.PRAC_ELEMENT); 

		for (int pracID = 0; pracID < pracNodeList.getLength(); pracID++)
		{
			Node pracNode = pracNodeList.item(pracID);

			if (pracNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element pracElement = (Element) pracNode;
				
				DataBaseXmlElementPracticalClasses practic = new DataBaseXmlElementPracticalClasses(pracElement, null);
				
				practic.setNamePractical(pracElement.getAttribute(ConstantsTools.PRAC_ELEMENT_NAME));
		
				
				
				pracList.add(practic);
			}
		}
		
		return pracList;
	}
	
	@SuppressWarnings("unused")
	private ArrayList<Laboratory> getLaboratory(Element groupElement)
	{
		ArrayList<Laboratory> labList = new ArrayList<Laboratory>();
		
		NodeList labNodeList = groupElement.getElementsByTagName(ConstantsTools.LAB_ELEMENT); 

		for (int labID = 0; labID < labNodeList.getLength(); labID++)
		{
			Node labNode = labNodeList.item(labID);

			if (labNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element labElement = (Element) labNode;
				
				DataBaseXmlElementLaboratory lab = new DataBaseXmlElementLaboratory(labElement, null);
				
				lab.setNameLab(labElement.getAttribute(ConstantsTools.LAB_ELEMENT_NAME)); 
		
				
				labList.add(lab);
			}
		}
		
		return labList;
	}
	@SuppressWarnings("unused")
	private ArrayList<Test> getTest(Element groupElement)
	{
		ArrayList<Test> totalyList = new ArrayList<Test>();
		NodeList testNodeList = groupElement.getElementsByTagName(ConstantsTools.TEST_ELEMENT); 

		for (int testID = 0; testID < testNodeList.getLength(); testID++)
		{
			Node testNode = testNodeList.item(testID);

			if (testNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element testElement = (Element) testNode;
				
				DataBaseXmlElementTest test = new DataBaseXmlElementTest(testElement, null);
				
				test.setNameTest(testElement.getAttribute(ConstantsTools.TEST_ELEMENT_NAME)); 
			
				
				
				totalyList.add(test);
			}
		}
		
	
	
		
		return totalyList;
	}
	
	/**
	 * Получить существующие дисциплины.
	 */
	public ArrayList<Discipline> getAllDiscipline()
	{
		return disList;
	}


}
