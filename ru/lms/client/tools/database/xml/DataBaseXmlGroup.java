package ru.lms.client.tools.database.xml;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import ru.lms.client.tools.ConstantsTools;
import ru.lms.client.tools.Group;
import ru.lms.client.tools.Laboratory;
import ru.lms.client.tools.Lecture;
import ru.lms.client.tools.PracticalClasses;
import ru.lms.client.tools.Student;
import ru.lms.client.tools.Test;
import ru.lms.client.tools.database.DataBaseGroupInt;
import ru.lms.client.tools.database.elements.xml.DataBaseXmlElementGroup;
import ru.lms.client.tools.database.elements.xml.DataBaseXmlElementLaboratory;
import ru.lms.client.tools.database.elements.xml.DataBaseXmlElementLecture;
import ru.lms.client.tools.database.elements.xml.DataBaseXmlElementPracticalClasses;
import ru.lms.client.tools.database.elements.xml.DataBaseXmlElementStudent;
import ru.lms.client.tools.database.elements.xml.DataBaseXmlElementTest;

/**
 * Xml- база данных для списка студентов.
 */
public class DataBaseXmlGroup extends DataBaseXml implements DataBaseGroupInt
{

	private static final Logger log = LoggerFactory.getLogger(DataBaseXmlGroup.class);
	
	private ArrayList<Group> groupList;	
	
	public DataBaseXmlGroup(String pathXml)
	{
		super(pathXml+"\\GroupsList.xml");
	}
	
	@Override
	public void openDB()
	{

		if(!this.xmlFile.exists())
		{
			PrintWriter writer = null;
			try
			{
				writer = new PrintWriter(this.pathXmlFile, "UTF-8");
				writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
				writer.println("<" + ConstantsTools.GROUP_ELEMENT + "s>");
				writer.println("</" +ConstantsTools.GROUP_ELEMENT+"s>");
				writer.close();
			} 
			catch (FileNotFoundException | UnsupportedEncodingException e)
			{
				log.error(e.getMessage());
				e.printStackTrace();
			}
		}

		super.openDB();
		
		this.readGroups();
	}
	
	private void readGroups()
	{
		log.info("Считывание всех групп...");

		this.groupList = new ArrayList<Group>();

		NodeList groupNodeList = this.xmlDataBase.getElementsByTagName(ConstantsTools.GROUP_ELEMENT);

		for (int je = 0; je < groupNodeList.getLength(); je++)
		{
			Node groupNode = groupNodeList.item(je);

			if (groupNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element groupElement = (Element) groupNode;
				
				DataBaseXmlElementGroup group = new DataBaseXmlElementGroup(groupElement, this);
				
				log.info("Считывание студентов в группе...");
				
				group.setStudentsList(this.getStudents(groupElement));
			 
				this.groupList.add(group);
			}
		}
	}
	
	private ArrayList<Student> getStudents(Element groupElement)
	{
		ArrayList<Student> studList = new ArrayList<Student>();
		
		NodeList studNodeList = groupElement.getElementsByTagName(ConstantsTools.STUDENT_ELEMENT);

		for (int studID = 0; studID < studNodeList.getLength(); studID++)
		{
			Node studNode = studNodeList.item(studID);

			if (studNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element studElement = (Element) studNode;
				
				DataBaseXmlElementStudent student = new DataBaseXmlElementStudent(studElement,this);
				
				student.setLectureList(this.getLecture(studElement));
				student.setPracticalClassesList(this.getPracticalClasses(studElement));
				student.setLaboratoryList(this.getLaboratory(studElement));
				student.setTestList(this.getTest(studElement));
				
				studList.add(student);
			}
		}
		
		return studList;
	}
	
	private ArrayList<Lecture> getLecture(Element studElement)
	{
		ArrayList<Lecture> lecList = new ArrayList<Lecture>();
		
		NodeList lecNodeList = studElement.getElementsByTagName(ConstantsTools.LECTURE_ELEMENT);

		for (int lecID = 0; lecID < lecNodeList.getLength(); lecID++)
		{
			Node lecNode = lecNodeList.item(lecID);

			if (lecNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element lecElement = (Element) lecNode;
				
				DataBaseXmlElementLecture lecture = new DataBaseXmlElementLecture(lecElement, null);
				
				lecture.setNameLecture(lecElement.getAttribute(ConstantsTools.LECTURE_ELEMENT_NAME));
				lecture.setVisit(lecElement.getAttribute(ConstantsTools.LECTURE_VISIT_ATTRIBUTE));
				
				
				
				lecList.add(lecture);
			}
		}
		
		return lecList;
	}
	
	
	@SuppressWarnings("unused")
	private ArrayList<PracticalClasses> getPracticalClasses(Element groupElement)
	{
		ArrayList<PracticalClasses> pracList = new ArrayList<PracticalClasses>();
		
		NodeList pracNodeList = groupElement.getElementsByTagName(ConstantsTools.PRAC_ELEMENT); 

		for (int pracID = 0; pracID < pracNodeList.getLength(); pracID++)
		{
			Node pracNode = pracNodeList.item(pracID);

			if (pracNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element pracElement = (Element) pracNode;
				
				DataBaseXmlElementPracticalClasses practic = new DataBaseXmlElementPracticalClasses(pracElement, null);
				
				practic.setNamePractical(pracElement.getAttribute(ConstantsTools.PRAC_ELEMENT_NAME));
				practic.setVisit(pracElement.getAttribute(ConstantsTools.PRAC_VISIT_ATTRIBUTE));
				
				
				pracList.add(practic);
			}
		}
		
		return pracList;
	}
	
	@SuppressWarnings("unused")
	private ArrayList<Laboratory> getLaboratory(Element groupElement)
	{
		ArrayList<Laboratory> labList = new ArrayList<Laboratory>();
		
		NodeList labNodeList = groupElement.getElementsByTagName(ConstantsTools.LAB_ELEMENT); 

		for (int labID = 0; labID < labNodeList.getLength(); labID++)
		{
			Node labNode = labNodeList.item(labID);

			if (labNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element labElement = (Element) labNode;
				
				DataBaseXmlElementLaboratory lab = new DataBaseXmlElementLaboratory(labElement, null);
				
				lab.setNameLab(labElement.getAttribute(ConstantsTools.LAB_ELEMENT_NAME)); 
				lab.setMarkLab((labElement.getAttribute(ConstantsTools.MARK_ATTRIBUTE)));
				
				labList.add(lab);
			}
		}
		
		return labList;
	}
	@SuppressWarnings("unused")
	private ArrayList<Test> getTest(Element groupElement)
	{
		ArrayList<Test> totalyList = new ArrayList<Test>();
		NodeList testNodeList = groupElement.getElementsByTagName(ConstantsTools.TEST_ELEMENT); 

		for (int testID = 0; testID < testNodeList.getLength(); testID++)
		{
			Node testNode = testNodeList.item(testID);

			if (testNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element testElement = (Element) testNode;
				
				DataBaseXmlElementTest test = new DataBaseXmlElementTest(testElement, null);
				
				test.setNameTest(testElement.getAttribute(ConstantsTools.TEST_ELEMENT_NAME)); 
				test.setMark((testElement.getAttribute(ConstantsTools.MARK_ATTRIBUTE)));
				
				
				totalyList.add(test);
			}
		}
		
	
	
		
		return totalyList;
	}

	@Override
	public ArrayList<Group> getGroups()
	{
		return this.groupList;
	}

	/* (non-Javadoc)
	 * @see ru.lms.client.tools.database.DataBaseGroupInt#addStudentToDB(ru.lms.client.tools.Student)
	 */
	@Override
	public Student addStudentToDB(Student student) {
	
		
		return null;
	}

}
