package ru.lms.client.tools.database;

import java.util.ArrayList;

import ru.lms.client.tools.Group;
import ru.lms.client.tools.Student;

/**
 * БД для списка студентов.
 */
public interface DataBaseGroupInt extends DataBaseInt
{
	public ArrayList<Group> getGroups();
	public Student addStudentToDB(Student student);
}
