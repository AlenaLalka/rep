package ru.lms.client.tools.database;

/**
 * Интерфейс для баз данных.
 */
public interface DataBaseInt
{
	/**Открывает базу данных для чтения/записи*/
	void openDB();
	/**Закрывет базу данных.*/
	void closeDB();
	/**Сохроняет базу данных.*/
	void saveDB();
	/**Обновить базу данных.*/
	void updateDB();
}
