package ru.lms.client.tools;

/**
 * Константы для пакеджа ru.lms.tools.
 */
public interface ConstantsTools
{
	/**Корень xml базы.*/
	public static final String ROOT_ELEMENT = "dataBaseXml";
	
	//=============Элементы для аккаунта===================
	public static final String ACCOUNT_LOGIN_ATTRIBUTE = "login";
	public static final String ACCOUNT_PASSWORD_ATTRIBUTE = "password";
	
	//=============Элементы для преподавателя===================
	public static final String TEACHER_ELEMENT = "teacher";	
	public static final String TEACHER_FIRST_NAME_ATTRIBUTE = "firstName";
	public static final String TEACHER_SECOND_NAME_ATTRIBUTE = "secondName";
	public static final String TEACHER_EMAIL_ATTRIBUTE = "eMail";
	public static final String TEACHER_WORKSPACE_PATH_ATTRIBUTE = "workspace";
	
	//=============Элементы для студентов===================
	public static final String STUDENT_ELEMENT = "student";	
	public static final String STUDENT_FIRST_NAME_ATTRIBUTE = "firstName";
	public static final String STUDENT_SECOND_NAME_ATTRIBUTE = "secondName";
	public static final String STUDENT_ID_ELEMENT = "id";
	public static final String STUDENT_POINT_ELEMENT = "point";
	public static final String STUDENT_POINT_LAB_ELEMENT = "pointLab";

	//===============Элементы для дисциплин==================
	public static final String DISCIPLINE_ELEMENT = "Discipline"; 
	public static final String DISCIPLINE_ELEMENT_NAME = "name"; 
	
	//===============Элементы для групп==================
	public static final String GROUP_ELEMENT = "group"; 
	public static final String GROUP_ELEMENT_NAME = "name"; 
	
	//===============Элементы для лекций==================
	public static final String LECTURE_ELEMENT = "lecture"; 
	public static final String LECTURE_ELEMENT_NAME = "name"; 
	public static final String LECTURE_VISIT_ATTRIBUTE = "visit";
	
	//===============Элементы для практик==================
	public static final String PRAC_ELEMENT = "practical"; 
	public static final String PRAC_ELEMENT_NAME = "name"; 
	public static final String PRAC_VISIT_ATTRIBUTE = "visit";
	
	//===============Элементы для лаб, тестов==================
	public static final String TOTALY_ELEMENT = "totaly"; 
	public static final String TOTALY_ELEMENT_NAME = "name"; 
	public static final String BONUS_ELEMENT = "bonus"; 
	public static final String BONUS_ELEMENT_NAME = "name";
	public static final String MARK_ATTRIBUTE = "mark";
	public static final String LAB_ELEMENT = "laboratory"; 
	public static final String LAB_ELEMENT_NAME = "name"; 
	public static final String TEST_ELEMENT = "test";
	public static final String TEST_ELEMENT_NAME = "name"; 
	
	//===============Элементы для учебного плана==================
	public static final String CURR_COUNT_STUD = "CountStud"; 
	public static final String CURR_NAME_GROUP_ELEMENT = "NameGroup"; 
	public static final String CURR_LEC_ELEMENT = "CountLec"; 
	public static final String CURR_TEST_ELEMENT = "CountTest";
	public static final String CURR_PRAC_ELEMENT = "CountPrac";
	public static final String CURR_LAB_ELEMENT = "CountLab";
	public static final String CURR_POINT_LEC_ELEMENT = "PointLec";
	public static final String CURR_POINT_PRAC_ELEMENT = "PointPrac";
	public static final String CURR_POINT_LAB_ELEMENT = "PointLab";
	public static final String CURR_POINT_TEST_ELEMENT = "PointTest";
	public static final String CURR_POINT_TOTALITTY_ELEMENT = "PointTotalitty";
	
	
	//===============Элементы для лабораторной==================
	public static final String LAB_NUMBER_ELEMENT = "NumberLab"; 
	public static final String LAB_NAME_ELEMENT = "NameLab"; 
	public static final String LAB_POINTDEAD_ELEMENT = "PointLab"; 
	public static final String LAB_POINTAFTER_ELEMENT = "PointAfterDeadLine";
	public static final String LAB_DATEDEADL_ELEMENT = "DateDeadLine";
	
}
