package ru.lms.client.tools;

import ru.lms.client.tools.database.elements.DataBaseElementLecture;


public class Lecture extends DataBaseElementLecture
{
	protected String nameLecture;
	
	protected String visit;
	
	protected int markLec;
	
	public Lecture()
	{
		
	}

	public String getNameLecture()
	{
		return nameLecture;
	}

	public void setNameLecture(String nameLecture)
	{
		this.nameLecture = nameLecture;
	}

	public String getVisit()
	{
		return visit;
	}

	public void setVisit(String visit)
	{
		this.visit = visit;
	}

	public int getMarkLec() {
		return markLec;
	}

	public void setMarkLec(int markLec) {
		this.markLec = markLec;
	}
}
