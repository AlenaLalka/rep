package ru.lms.client.tools;

import java.util.ArrayList;

import ru.lms.client.tools.database.elements.DataBaseElementGroup;

/**
 * Структура для групп. */
public class Group extends DataBaseElementGroup
{
	protected String nameGroup;
	
	/**Всего баллов за семестр*/
	protected int totaly;
	
	protected ArrayList<Student> studentsList;
	
	protected ArrayList<Lecture> lectureList;
	
	protected ArrayList<PracticalClasses> practicalList;
	
	protected ArrayList<Laboratory> labList;
	
	protected ArrayList<Test> testList;
	
	public Group(String nameGroup, ArrayList<Student> studentsList)
	{
		this.nameGroup = nameGroup;
		this.studentsList = new ArrayList<Student>();
		this.lectureList = new ArrayList<Lecture>();
		this.practicalList = new ArrayList<PracticalClasses>();
		this.labList = new ArrayList<Laboratory>();
		this.testList = new ArrayList<Test>();
		this.setStudentsList(studentsList);
	}
	
	public Group()
	{
		this(null,null);
	}

	public void addStudent(Student student)
	{
		this.studentsList.add(student);
		student.setLectureList(lectureList);
		student.setPracticalClassesList(practicalList);
		student.setLaboratoryList(labList);
		student.setTestList(testList);
	}
	
	public String getNameGroup()
	{
		return nameGroup;
	}

	public void setNameGroup(String nameGroup)
	{
		this.nameGroup = nameGroup;
	}
	
	public ArrayList<Lecture> getLectureList()
	{
		return lectureList;
	}

	public ArrayList<PracticalClasses> getPracticalClassesList()
	{
		return practicalList;
	}
	
	public void setLectureList(ArrayList<Lecture> lectureList)
	{
		if(lectureList== null)
			return;
		
		this.lectureList.clear();
		for(Lecture lec : lectureList)
			this.lectureList.add(lec);
	}


	public void setPracticalClassesList(ArrayList<PracticalClasses> practicalList)
	{
		if(practicalList== null)
			return;
		
		this.practicalList.clear();
		for(PracticalClasses prac : practicalList)
			this.practicalList.add(prac);
	}
	public ArrayList<Student> getStudentsList()
	{
		return studentsList;
	}
	
	public void addLectureToStud()
	{
		/*for(Student stud : this.studentsList)
		{
			stud.setLectureList(lectureList);
		}*/
	}

	public void addPracticalClassesToStud()
	{
	/*	for(Student stud : this.studentsList)
		{
			stud.setPracticalClassesList(practicalList);
		}*/
	}
	public void setStudentsList(ArrayList<Student> studentsList)
	{
		if(studentsList!=null)
		{		
			this.studentsList.clear();
			
			for(Student stud : studentsList)
				this.studentsList.add(stud);
		}
	}
	
	public ArrayList<Test> getTestList()
	{
		return testList;
	}

	public void setTestList(ArrayList<Test> testList)
	{
		if(testList== null)
			return;
		
		this.testList.clear();
		for(Test ts : testList)
			this.testList.add(ts);
	}
	
	public void setLaboratoryList(ArrayList<Laboratory> labList)
	{
		if(labList== null)
			return;
		
		this.labList.clear();
		for(Laboratory lab : labList)
			this.labList.add(lab);
	}
	
	public ArrayList<Laboratory> getLaboratoryList()
	{
		return labList;
	}
}
