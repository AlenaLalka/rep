package ru.lms.client.tools;

import java.util.ArrayList;

import ru.lms.client.tools.database.elements.DataBaseElementTeacher;

/**
 * Структура для преподавателей.
 */
public class Teacher extends DataBaseElementTeacher
{

	/**Имя.*/
	protected String firstName;
	/**Фамилия.*/
	protected String secondName;
	/**E-mail.*/
	protected String eMail;
	/**Рабочая папка*/
	protected String workspacePath;
	
	protected ArrayList<Discipline> disciplineList;
	
	protected ArrayList<Student> studentsList;
	
	public Teacher(String login, 
			String pass,
			String firstName, 
			String secontName, 
			String eMail,
			String workspacePath)
	{
		super(login, pass);
		
		this.firstName = firstName;
		this.secondName = secontName;
		this.eMail = eMail;
		this.workspacePath = workspacePath;
		this.disciplineList = new ArrayList<Discipline>();
		
	}
	
	public Teacher()
	{
		this("NULL", "NULL" , "NULL" , "NULL" , "NULL" ,"NULL");
	}
	
	//===================GET & SET==================
	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getSecondName()
	{
		return secondName;
	}

	public void setSecondName(String secondName)
	{
		this.secondName = secondName;
	}

	public String getWorkspacePath()
	{
		return workspacePath;
	}

	public void setWorkspacePath(String workspacePath)
	{
		this.workspacePath = workspacePath;
	}

	public String geteMail()
	{
		return eMail;
	}

	public void seteMail(String eMail)
	{
		this.eMail = eMail;
	}

	public ArrayList<Discipline> getDisciplineList()
	{
		return disciplineList;
	}

	public void setDisciplineList(ArrayList<Discipline> disciplineList)
	{
		this.disciplineList.clear();
		
		for(Discipline dic : disciplineList)
			this.disciplineList.add(dic);
	}

	
	
	
}
