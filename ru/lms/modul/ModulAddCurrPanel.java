package ru.lms.modul;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import com.alee.extended.date.WebDateField;
import com.alee.extended.painter.DashedBorderPainter;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.text.WebTextField;
import com.alee.managers.style.skin.web.WebLabelPainter;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.lms.client.teacher.FrameTeacherClient;
import ru.lms.client.teacher.gui.TabsPanelModul;
import ru.lms.client.tools.Discipline;
import ru.lms.client.tools.Group;
import ru.lms.client.tools.Teacher;
import ru.lms.lang.Language;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.SwingConstants;
/**Модуль для добавления нового учебного плана группы :Р*/
@SuppressWarnings("serial")
public class ModulAddCurrPanel extends Modul{
	private static final Logger log = LoggerFactory.getLogger(ModulAddCurrPanel.class);
	private WebPanel mainPanel;
	private WebTextField tNameGroup;
	private WebTextField tCountLec;
	private WebTextField tCountPrac;
	private WebTextField tCountLab;
	private WebTextField tCountTest;
	private WebTextField tPointLec;
	private WebTextField tPointPrac;
	private WebLabel lCreateCurr;
	private WebTextField tTotalyPoint;
	private TabsPanelModul tabsPanelModul;
	private WebTextField tnumberGroup;
	private String discipline;


	  public  ModulAddCurrPanel()
	{   
		
		this.mainPanel = new WebPanel();
		mainPanel.setBackground(new Color(255, 255, 255));
		
		this.init();
		
	}
	
	  public  ModulAddCurrPanel(TabsPanelModul tabsPanelModul,String nameDis)
		{   	
		    this.discipline = nameDis;
			this.tabsPanelModul = tabsPanelModul;
			this.mainPanel = new WebPanel();
			mainPanel.setBackground(new Color(255, 255, 255));
			this.init();	
		}
	

	private void init()
	{
		this.nameModul = "Добавление учебного плана";
	    
		log.info("Открывыта панель для добавления плана учебного");
		
		this.add(mainPanel,BorderLayout.CENTER);
		//mainPanel.setBackground(Color.WHITE);
		
		WebLabel lNameGroup = new WebLabel(Language.LABEL_ENTER_NAME_GROUP);
		lNameGroup.setForeground(UIManager.getColor("Button.foreground"));
		lNameGroup.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));

		
		WebLabel lCountLec = new WebLabel(Language.LABEL_COUNT_LEC_CURR);
		lCountLec.setForeground(UIManager.getColor("Button.foreground"));
		lCountLec.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));

		
		WebLabel lCountPrac = new WebLabel(Language.LABEL_ENTER_PRAC_CURR);
		lCountPrac.setForeground(UIManager.getColor("Button.foreground"));
		lCountPrac.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));

		
		WebLabel lCountLab = new WebLabel(Language.LABEL_ENTER_LAB_CURR);
		lCountLab.setForeground(UIManager.getColor("Button.foreground"));
		lCountLab.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));

		
		WebLabel lCountTest = new WebLabel(Language.LABEL_ENTER_TEST_CURR);
		lCountTest.setForeground(UIManager.getColor("Button.foreground"));
		lCountTest.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		
		WebLabel lblNewLabel = new WebLabel("Итого за семстр: ");
		lblNewLabel.setForeground(UIManager.getColor("Button.foreground"));
		lblNewLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));

		
		tNameGroup = new WebTextField();
		tNameGroup.setColumns(10);
		
		tCountLec = new WebTextField();
		tCountLec.setColumns(10);
		
		tCountPrac = new WebTextField();
		tCountPrac.setColumns(10);
		
		tCountLab = new WebTextField();
		tCountLab.setColumns(10);
		
		tCountTest = new WebTextField();
		tCountTest.setColumns(10);
		
		tPointLec = new WebTextField();
		tPointLec.setColumns(10);
		
		tPointPrac = new WebTextField();
		tPointPrac.setColumns(10);
		
		tTotalyPoint = new WebTextField();
		tTotalyPoint.setColumns(10);
		
		lCreateCurr = new WebLabel(Language.LABEL_TITEL_ADD_CURR);
		final DashedBorderPainter bp4 = new DashedBorderPainter ( new float[]{ 3f, 3f } );  // украшалочка :3
        bp4.setRound ( 12 );
        bp4.setWidth ( 2 );
        bp4.setColor ( new Color ( 39, 95, 173 ) );
        lCreateCurr.setPainter ( new WebLabelPainter ( bp4 ) ).setMargin ( 10 );
        lCreateCurr.setForeground(UIManager.getColor("TitledBorder.titleColor"));
        lCreateCurr.setFont(new Font("comic sans ms", Font.PLAIN, 19));
		
        
		WebButton bNext = new WebButton(Language.BUTTTON_NEXT);
		bNext.setFontSize(12);
		
		WebButton bCancel = new WebButton(Language.BUTTTON_CANCEL);
		bCancel.setVerticalAlignment(SwingConstants.BOTTOM);
		bCancel.setFontSize(12);
		
		tnumberGroup = new WebTextField();
		tnumberGroup.setColumns(10);
		
		WebLabel webLabel_1 = new WebLabel("Балл за посещение: ");
		webLabel_1.setText("Балл: ");
		webLabel_1.setForeground(Color.BLACK);
		webLabel_1.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		
		WebLabel curse = new WebLabel("Имя группы: ");
		curse.setVerticalAlignment(SwingConstants.TOP);
		curse.setText("Курс:");
		curse.setForeground(Color.BLACK);
		curse.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		GroupLayout gl_mainPanel = new GroupLayout(mainPanel);
		gl_mainPanel.setHorizontalGroup(
			gl_mainPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_mainPanel.createSequentialGroup()
					.addContainerGap(585, Short.MAX_VALUE)
					.addComponent(bNext, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(bCancel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(175))
				.addGroup(gl_mainPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(curse, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(782, Short.MAX_VALUE))
				.addGroup(gl_mainPanel.createSequentialGroup()
					.addGroup(gl_mainPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_mainPanel.createSequentialGroup()
							.addGap(10)
							.addGroup(gl_mainPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
								.addComponent(lCountTest, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
								.addComponent(lCountPrac, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lCountLec, GroupLayout.PREFERRED_SIZE, 224, GroupLayout.PREFERRED_SIZE)
								.addComponent(lCountLab, GroupLayout.PREFERRED_SIZE, 224, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_mainPanel.createSequentialGroup()
							.addContainerGap()
							.addComponent(lNameGroup, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)))
					.addGap(144)
					.addGroup(gl_mainPanel.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_mainPanel.createParallelGroup(Alignment.LEADING)
							.addComponent(tCountTest, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(tTotalyPoint, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_mainPanel.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(tCountPrac, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(tCountLec, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(tNameGroup, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(tnumberGroup, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
						.addComponent(tCountLab, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(webLabel_1, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_mainPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(tPointLec, GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE)
						.addComponent(tPointPrac, GroupLayout.PREFERRED_SIZE, 59, GroupLayout.PREFERRED_SIZE))
					.addGap(293))
				.addGroup(gl_mainPanel.createSequentialGroup()
					.addGap(28)
					.addComponent(lCreateCurr, GroupLayout.PREFERRED_SIZE, 414, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(447, Short.MAX_VALUE))
		);
		gl_mainPanel.setVerticalGroup(
			gl_mainPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_mainPanel.createSequentialGroup()
					.addGap(25)
					.addComponent(lCreateCurr, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
					.addGap(48)
					.addGroup(gl_mainPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_mainPanel.createSequentialGroup()
							.addGap(2)
							.addGroup(gl_mainPanel.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_mainPanel.createSequentialGroup()
									.addComponent(lNameGroup, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(curse, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_mainPanel.createSequentialGroup()
									.addComponent(tNameGroup, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(tnumberGroup, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_mainPanel.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_mainPanel.createParallelGroup(Alignment.BASELINE)
									.addComponent(tCountLec, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(lCountLec, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addComponent(tPointLec, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_mainPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(tCountPrac, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lCountPrac, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(tPointPrac, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(7)
							.addGroup(gl_mainPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(tCountLab, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lCountLab, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_mainPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(lCountTest, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(tCountTest, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_mainPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(tTotalyPoint, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_mainPanel.createSequentialGroup()
							.addGap(82)
							.addComponent(webLabel_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(37)
					.addGroup(gl_mainPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(bNext, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(bCancel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addGap(13))
		);
		mainPanel.setLayout(gl_mainPanel);
		
		
		
		
		bCancel.addActionListener( new ActionListener ()
        {
            
            public void actionPerformed ( ActionEvent e )
            {
            	int c = tabsPanelModul.getTabCount();
				c--;
				tabsPanelModul.remove(c);
            }
        });
		
		
	
		
		bNext.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				int c = tabsPanelModul.getTabCount();
				c--;
				tabsPanelModul.remove(c);
				ModulCurrTablePanel pf = new ModulCurrTablePanel( tabsPanelModul, discipline, gettNameGroup(), gettNumberGroup(), gettCountLec(), gettCountPrac(), gettCountLab(), gettCountTest(), gettPointTotaly());
				tabsPanelModul.registerObserverModul(pf);
				pf.update(activTeacher);

			}
		});
		
		
	}
	
	public WebPanel getPanel(){
		return mainPanel;
		
	}
	

	@Override
	public void update() {
		
		
	}
    public String gettNumberGroup(){
    	return tnumberGroup.getText();
    }
	public String gettNameGroup() {
		return tNameGroup.getText();
	}

	public String gettCountLec() {
		return tCountLec.getText();
	}

	public String gettCountPrac() {
		return tCountPrac.getText();
	}

	public String gettCountLab() {
		return tCountLab.getText();
	}

	public String gettCountTest() {
		return tCountTest.getText();
	}

	public String gettPointLec() {
		return tPointLec.getText();
	}

	public String gettPointPrac() {
		return tPointPrac.getText();
	}
	
	public String gettPointTotaly() {
		return tTotalyPoint.getText();
	}
	
	/*public String gettNumber() {
		return tnumberGroup.getText();
	}*/
	
	public void update(Group group)
	{
		super.update(group);
	}
	public void update(Discipline discipline) {
		super.update(discipline);
	}
	public void update(Teacher teacher) {
		super.update(teacher);
	}
}
