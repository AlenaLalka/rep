package ru.lms.modul;

import java.awt.Color;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.lms.client.teacher.FrameTeacherClient;
import ru.lms.client.tools.Group;
import ru.lms.client.tools.Laboratory;
import ru.lms.client.tools.PracticalClasses;
import ru.lms.client.tools.Student;
import ru.lms.client.tools.Test;
import ru.lms.lang.Language;

import com.alee.laf.button.WebButton;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.table.WebTable;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;
/** 
 * 
 * Модуль-таблица для отображения успеваемости по лаб-м, тестам 
 * */
@SuppressWarnings("serial")
public class ModulLabTestTotaly extends Modul
{
	private static final Logger log = LoggerFactory.getLogger(ModulLabTestTotaly.class);

	private WebTable table;
	private WebPanel mainPanel;
	private DefaultTableModel modelTable;
	private WebPanel panel_1;

	private String XmlFileGroup;
	private FrameTeacherClient mainFrame;
	private WebButton cancel;

	public ModulLabTestTotaly()
	{
		

		this.mainPanel = new WebPanel();
		mainPanel.setBackground(new Color(255, 255, 255));
		this.init();

	}

	public ModulLabTestTotaly(String nameModul)
	{
		this.nameModul = nameModul;
		this.mainPanel = new WebPanel();
		mainPanel.setBackground(new Color(255, 255, 255));

		this.init();
	}

    private void init()
	
	{
    	this.add(mainPanel,BorderLayout.CENTER);

    	mainPanel.setBackground(Color.WHITE);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(mainPanel, GroupLayout.DEFAULT_SIZE, 747, Short.MAX_VALUE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(mainPanel, GroupLayout.DEFAULT_SIZE, 432, Short.MAX_VALUE)
		);
		
		panel_1 = new WebPanel();
		

		
		WebButton btnNewButton_1 = new WebButton();
		btnNewButton_1.setText("Создать отчет");
		
		WebButton btnNewButton_2 = new WebButton();
		btnNewButton_2.setText("Загрузить список студентов");
		
		cancel = new WebButton();
		
		cancel.setText("Закрыть");
		GroupLayout gl_panel = new GroupLayout(mainPanel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 725, Short.MAX_VALUE)
						.addGroup(gl_panel.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED, 208, Short.MAX_VALUE)
							.addComponent(btnNewButton_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 182, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(cancel, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(14)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(cancel, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnNewButton_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
					.addContainerGap())
		);
		panel_1.setLayout(new BorderLayout(0, 0));
		mainPanel.setLayout(gl_panel);
		setLayout(groupLayout);
        
		/*int colLAB =0;
		int colTEST =0;

*/
		modelTable = new DefaultTableModel(); 
		table= new WebTable(modelTable);
		
		table.setBackground(Color.WHITE);
		table.setColumnSelectionAllowed(true);
		table.setRowSelectionAllowed(true);
	    modelTable.setRowCount(0);


		
	   

	    	panel_1.add(new WebScrollPane(table),BorderLayout.CENTER);
		
            panel_1.setVisible(true); 	
      
            
	}
    public void update(Group group)
	{
		super.update(group);
		
		DefaultTableModel tableModel = new DefaultTableModel();
		
		tableModel.addColumn(group.getNameGroup());
		
		for(Laboratory lab : group.getLaboratoryList())
		{
			tableModel.addColumn(lab.getNameLab());
		}
		
		for(Test test : group.getTestList())
		{
			tableModel.addColumn(test.getNameTest());
		}
		
		tableModel.addColumn("Итого");
		
		for(Student stud : group.getStudentsList())
		{
			Vector<String> rowData = new Vector<String>();
			rowData.add(stud.getFirstName()+" "+stud.getSecondName());
			
			for(Laboratory lb : stud.getLaboratoryList())
			{
			
				rowData.add((lb.getMarkLab()));
			}
			
			for(Test ts : stud.getTestList())
			{
			
				rowData.add((ts.getMark()));
			}
			
			tableModel.addRow(rowData);
		}
		
		this.table.setModel(tableModel);
		
		
	}          
             

public WebPanel getPanel()
{
	return mainPanel;
}
	
	public void update() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @param actionListener
	 */
	public void setCloseAction(ActionListener action) {
		cancel.addActionListener(action);
	}
}
