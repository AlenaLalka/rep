package ru.lms.modul;

import java.awt.Component;

/**
 * Интерфейс для модулей.
 */
public interface ModulInt
{
	/**Обновлеем содержимое модуля.*/
	void update();
	
}
