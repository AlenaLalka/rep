package ru.lms.modul;

import java.awt.Color;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.lms.client.teacher.FrameTeacherClient;
import ru.lms.client.teacher.gui.TabsPanelModul;
import ru.lms.client.tools.Group;
import ru.lms.client.tools.Laboratory;
import ru.lms.client.tools.Lecture;
import ru.lms.client.tools.PracticalClasses;
import ru.lms.client.tools.Teacher;
import ru.lms.client.tools.Test;
import ru.lms.lang.Language;

import com.alee.extended.button.WebSwitch;
import com.alee.extended.date.WebDateField;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.table.WebTable;
import com.alee.laf.table.renderers.WebTableCellRenderer;
import com.alee.laf.text.WebTextField;
import com.alee.utils.swing.WebDefaultCellEditor;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;
import javax.xml.crypto.Data;

@SuppressWarnings("serial")
public class ModulCurrTablePanel extends Modul {
	private static final Logger log = LoggerFactory
			.getLogger(ModulCurrTablePanel.class);
	private WebPanel mainPanel;
	private WebPanel panelLab;
	private WebPanel panelTest;
	private WebTable tabllab;
	private WebTable tabltest;
	private DefaultTableModel modelTableLab;
	private DefaultTableModel modelTableTest;
	private WebButton cancel;

	private int collab;
	private int rowlab;
	private int rowtest;
	private WebTextField textField;
	private TabsPanelModul tabsPanelModul;
	private String nameGroup;
	private String nameDiscipline;
	private int countLec;
	private int countPrac;


	public ModulCurrTablePanel() throws ParseException {

		this.mainPanel = new WebPanel();
		this.init();
	}

	public ModulCurrTablePanel(TabsPanelModul tabsPanelModul, String discip,
			String NameGroup, String nGroup, String cLec, String cPr,
			String cLab, String cTest, String tot) {
		this.nameGroup = NameGroup + " " + nGroup + " курс";
		this.rowlab = Integer.parseInt(cLab);
		this.rowtest = Integer.parseInt(cTest);
		this.countLec = Integer.parseInt(cLec);
		this.countPrac = Integer.parseInt(cPr);
		this.nameDiscipline = discip;
		this.tabsPanelModul = tabsPanelModul;
		this.mainPanel = new WebPanel();
		this.init();
	}

	@SuppressWarnings("rawtypes")
	private void init()

	{
		this.add(mainPanel, BorderLayout.CENTER);
		this.setNameModul("Продолжение создания плана для группы " + nameGroup
				+ "а");
		final WebDateField dat = new WebDateField(new Date());
		final WebDateField dat2 = new WebDateField(new Date());

		textField = new WebTextField();
		textField.setToolTipText("Количество занятий в неделю");
		textField.setColumns(10);

		final WebSwitch ws1 = new WebSwitch();
		ws1.getComponentOrientation().toString();
		final WebSwitch ws2 = new WebSwitch();

		mainPanel.setBackground(Color.WHITE);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(
				Alignment.LEADING).addGroup(
				groupLayout
						.createSequentialGroup()
						.addComponent(mainPanel, GroupLayout.DEFAULT_SIZE, 812,
								Short.MAX_VALUE).addGap(0)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(
				Alignment.TRAILING).addComponent(mainPanel, Alignment.LEADING,
				GroupLayout.DEFAULT_SIZE, 420, Short.MAX_VALUE));

		panelLab = new WebPanel();

		panelTest = new WebPanel();

		WebLabel lblNewLabel_1 = new WebLabel(Language.LABEL_TOTAL_POINT);
		lblNewLabel_1.setFontSize(12);
		lblNewLabel_1.setText("Первое практическое занятие ");

		WebButton btnNewButton_1 = new WebButton(Language.BUTTTON_SAVE);

		cancel = new WebButton("Отмена");

		WebLabel webLabel = new WebLabel("Первое лекционное занятие");
		webLabel.setFontSize(12);

		WebLabel webLabel_1 = new WebLabel("*");

		WebLabel webLabel_2 = new WebLabel("*");

		WebLabel webLabel_3 = new WebLabel("через неделю ");
		webLabel_3.setFontSize(12);

		WebLabel webLabel_4 = new WebLabel("через неделю ");
		webLabel_4.setFontSize(12);

		WebTextField webTextField = new WebTextField();
		webTextField.setToolTipText("Количество занятий в неделю");
		webTextField.setColumns(10);

		GroupLayout gl_panel = new GroupLayout(mainPanel);
		gl_panel.setHorizontalGroup(gl_panel
				.createParallelGroup(Alignment.LEADING)
				.addGroup(
						gl_panel.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										gl_panel.createParallelGroup(
												Alignment.LEADING)
												.addComponent(
														panelLab,
														Alignment.TRAILING,
														GroupLayout.DEFAULT_SIZE,
														646, Short.MAX_VALUE)
												.addComponent(
														panelTest,
														Alignment.TRAILING,
														GroupLayout.DEFAULT_SIZE,
														646, Short.MAX_VALUE)
												.addGroup(
														gl_panel.createSequentialGroup()
																.addGroup(
																		gl_panel.createParallelGroup(
																				Alignment.LEADING,
																				false)
																				.addComponent(
																						webLabel,
																						Alignment.TRAILING,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE)
																				.addComponent(
																						lblNewLabel_1,
																						Alignment.TRAILING,
																						GroupLayout.PREFERRED_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.PREFERRED_SIZE))
																.addGap(18)
																.addGroup(
																		gl_panel.createParallelGroup(
																				Alignment.TRAILING,
																				false)
																				.addComponent(
																						dat,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE)
																				.addComponent(
																						dat2,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE))
																.addGap(18)
																.addGroup(
																		gl_panel.createParallelGroup(
																				Alignment.LEADING,
																				false)
																				.addComponent(
																						webLabel_1,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE)
																				.addComponent(
																						webLabel_2,
																						GroupLayout.PREFERRED_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.PREFERRED_SIZE))
																.addGap(10)
																.addGroup(
																		gl_panel.createParallelGroup(
																				Alignment.LEADING,
																				false)
																				.addComponent(
																						webTextField,
																						0,
																						0,
																						Short.MAX_VALUE)
																				.addComponent(
																						textField,
																						GroupLayout.PREFERRED_SIZE,
																						30,
																						GroupLayout.PREFERRED_SIZE))
																.addGap(18)
																.addGroup(
																		gl_panel.createParallelGroup(
																				Alignment.LEADING)
																				.addComponent(
																						webLabel_3,
																						GroupLayout.PREFERRED_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.PREFERRED_SIZE)
																				.addComponent(
																						webLabel_4,
																						GroupLayout.PREFERRED_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.PREFERRED_SIZE))
																.addGroup(
																		gl_panel.createParallelGroup(
																				Alignment.LEADING)
																				.addGroup(
																						gl_panel.createSequentialGroup()
																								.addGap(9)
																								.addComponent(
																										ws2,
																										GroupLayout.PREFERRED_SIZE,
																										GroupLayout.DEFAULT_SIZE,
																										GroupLayout.PREFERRED_SIZE))
																				.addGroup(
																						gl_panel.createSequentialGroup()
																								.addGap(10)
																								.addComponent(
																										ws1,
																										GroupLayout.PREFERRED_SIZE,
																										GroupLayout.DEFAULT_SIZE,
																										GroupLayout.PREFERRED_SIZE))))
												.addGroup(
														Alignment.TRAILING,
														gl_panel.createSequentialGroup()
																.addComponent(
																		btnNewButton_1,
																		GroupLayout.PREFERRED_SIZE,
																		GroupLayout.DEFAULT_SIZE,
																		GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(
																		ComponentPlacement.UNRELATED)
																.addComponent(
																		cancel,
																		GroupLayout.PREFERRED_SIZE,
																		71,
																		GroupLayout.PREFERRED_SIZE)))
								.addContainerGap()));
		gl_panel.setVerticalGroup(gl_panel
				.createParallelGroup(Alignment.LEADING)
				.addGroup(
						gl_panel.createSequentialGroup()
								.addContainerGap()
								.addComponent(panelLab,
										GroupLayout.PREFERRED_SIZE, 103,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(panelTest,
										GroupLayout.PREFERRED_SIZE, 115,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(
										gl_panel.createParallelGroup(
												Alignment.TRAILING)
												.addGroup(
														gl_panel.createSequentialGroup()
																.addGroup(
																		gl_panel.createParallelGroup(
																				Alignment.TRAILING)
																				.addGroup(
																						Alignment.LEADING,
																						gl_panel.createSequentialGroup()
																								.addGroup(
																										gl_panel.createParallelGroup(
																												Alignment.LEADING)
																												.addGroup(
																														Alignment.TRAILING,
																														gl_panel.createSequentialGroup()
																																.addGroup(
																																		gl_panel.createParallelGroup(
																																				Alignment.BASELINE)
																																				.addComponent(
																																						dat,
																																						GroupLayout.PREFERRED_SIZE,
																																						GroupLayout.DEFAULT_SIZE,
																																						GroupLayout.PREFERRED_SIZE)
																																				.addComponent(
																																						webLabel,
																																						GroupLayout.PREFERRED_SIZE,
																																						GroupLayout.DEFAULT_SIZE,
																																						GroupLayout.PREFERRED_SIZE))
																																.addGap(18))
																												.addGroup(
																														Alignment.TRAILING,
																														gl_panel.createSequentialGroup()
																																.addPreferredGap(
																																		ComponentPlacement.RELATED,
																																		2,
																																		Short.MAX_VALUE)
																																.addGroup(
																																		gl_panel.createParallelGroup(
																																				Alignment.TRAILING)
																																				.addGroup(
																																						gl_panel.createParallelGroup(
																																								Alignment.BASELINE)
																																								.addComponent(
																																										textField,
																																										GroupLayout.PREFERRED_SIZE,
																																										GroupLayout.DEFAULT_SIZE,
																																										GroupLayout.PREFERRED_SIZE)
																																								.addComponent(
																																										webLabel_3,
																																										GroupLayout.PREFERRED_SIZE,
																																										21,
																																										GroupLayout.PREFERRED_SIZE))
																																				.addComponent(
																																						webLabel_1,
																																						GroupLayout.PREFERRED_SIZE,
																																						14,
																																						GroupLayout.PREFERRED_SIZE))
																																.addGap(18)))
																								.addGroup(
																										gl_panel.createParallelGroup(
																												Alignment.BASELINE,
																												false)
																												.addComponent(
																														lblNewLabel_1,
																														GroupLayout.PREFERRED_SIZE,
																														26,
																														GroupLayout.PREFERRED_SIZE)
																												.addComponent(
																														dat2,
																														GroupLayout.PREFERRED_SIZE,
																														GroupLayout.DEFAULT_SIZE,
																														GroupLayout.PREFERRED_SIZE)
																												.addComponent(
																														webLabel_2,
																														GroupLayout.PREFERRED_SIZE,
																														14,
																														GroupLayout.PREFERRED_SIZE)
																												.addComponent(
																														webTextField,
																														GroupLayout.PREFERRED_SIZE,
																														24,
																														GroupLayout.PREFERRED_SIZE)
																												.addComponent(
																														webLabel_4,
																														GroupLayout.PREFERRED_SIZE,
																														21,
																														GroupLayout.PREFERRED_SIZE)))
																				.addGroup(
																						gl_panel.createSequentialGroup()
																								.addGap(5)
																								.addComponent(
																										ws1,
																										GroupLayout.PREFERRED_SIZE,
																										GroupLayout.DEFAULT_SIZE,
																										GroupLayout.PREFERRED_SIZE)
																								.addGap(14)
																								.addComponent(
																										ws2,
																										GroupLayout.PREFERRED_SIZE,
																										GroupLayout.DEFAULT_SIZE,
																										GroupLayout.PREFERRED_SIZE)
																								.addGap(4)))
																.addGap(26))
												.addGroup(
														gl_panel.createParallelGroup(
																Alignment.BASELINE)
																.addComponent(
																		cancel,
																		GroupLayout.PREFERRED_SIZE,
																		GroupLayout.DEFAULT_SIZE,
																		GroupLayout.PREFERRED_SIZE)
																.addComponent(
																		btnNewButton_1,
																		GroupLayout.PREFERRED_SIZE,
																		GroupLayout.DEFAULT_SIZE,
																		GroupLayout.PREFERRED_SIZE)))));
		panelTest.setLayout(new BorderLayout(0, 0));
		panelLab.setLayout(new BorderLayout(0, 0));
		mainPanel.setLayout(gl_panel);
		setLayout(groupLayout);

		modelTableLab = new DefaultTableModel();
		modelTableTest = new DefaultTableModel();

		tabllab = new WebTable(modelTableLab);
		tabllab.setColumnSelectionAllowed(true);
		tabllab.setRowSelectionAllowed(true);

		tabltest = new WebTable(modelTableTest);
		tabltest.setColumnSelectionAllowed(true);
		tabltest.setRowSelectionAllowed(true);

		modelTableLab.setRowCount(rowlab);

		modelTableTest.setRowCount(rowtest);

		modelTableLab.addColumn("Лабораторная");
		modelTableLab.addColumn("Название лабораторной");
		modelTableLab.addColumn("Максимальный балл");
		modelTableLab.addColumn("Крайний срок сдачи");

		modelTableTest.addColumn("Тест");
		modelTableTest.addColumn("Название теста");
		modelTableTest.addColumn("Максимальный балл");

		tabllab.getRowHeight(40);
		tabllab.getColumnModel().getColumn(0).setMinWidth(150);
		tabltest.getColumnModel().getColumn(0).setMinWidth(150);
		tabltest.getColumnModel().getColumn(0).setMaxWidth(150);

		final TableColumn tb = tabllab.getColumnModel().getColumn(3);

		WebTableCellRenderer render = new WebTableCellRenderer();
		render.setToolTipText("Правее нажмите, появится календарик :3");
		tb.setCellRenderer(render);

		tb.setCellEditor(new WebDefaultCellEditor(new WebDateField(new Date())));

		for (int i = 0; i < rowlab; i++) {
			modelTableLab.setValueAt(i + 1, i, 0);
		}
		for (int i = 0; i < rowtest; i++) {
			modelTableTest.setValueAt(i + 1, i, 0);
		}

		panelLab.add(new WebScrollPane(tabllab), BorderLayout.CENTER);
		panelTest.add(new WebScrollPane(tabltest), BorderLayout.CENTER);

		ws1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println(ws1.isSelected());
			}
		});

		cancel.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int c = tabsPanelModul.getTabCount();
				c--;
				tabsPanelModul.remove(c);
			}
		});
		btnNewButton_1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				Group group = new Group();
				group.setNameGroup(nameGroup);
                
				ArrayList<Laboratory> labList = new ArrayList<Laboratory>();
				ArrayList<Lecture> lecList = new ArrayList<Lecture>();
				ArrayList<PracticalClasses> pracList = new ArrayList<PracticalClasses>();
				ArrayList<Test> testList = new ArrayList<Test>();

				Date lecDate = dat.getDate();
				Date prDate = dat2.getDate();

				Lecture lectire = new Lecture();
				PracticalClasses prac = new PracticalClasses();
				Laboratory lab = new Laboratory();
				Test test = new Test();

				for (int i = 0; i < countLec; i++) {

					String date = null;
					try {
						date = DateConvert(lecDate);
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}
					lectire.setNameLecture(date);
					lecList.add(lectire);
				}

				for (int i = 0; i < countPrac; i++) {
					String date = null;
					try {
						date = DateConvert(prDate);
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}
					prac.setNamePractical(date);
					pracList.add(prac);

				}

				for (int i = 0; i < rowlab; i++) {
					int col = 1;

					lab.setNameLab("Лаб. "+ (i++) +"( "+ modelTableLab.getValueAt(i, col).toString() +" )");
					lab.setMaxMark(modelTableLab.getValueAt(i, col++).toString());

					labList.add(lab);

				}

				for (int i = 0; i < rowtest; i++) {

					int col = 1;

					test.setNameTest("Лаб. "+ (i++) +"( "+ modelTableTest.getValueAt(i, col)
							.toString()+" )");
					test.setMaxmark(modelTableLab.getValueAt(i, col++)
							.toString());
					testList.add(test);

				}
				group.setLectureList(lecList);
				group.setLaboratoryList(labList);
				group.setPracticalClassesList(pracList);
				group.setTestList(testList);

				activTeacher.getDataBaseDisciplines().addGroupToDisciplineToDB( group, nameDiscipline, lecList, pracList, labList, testList);
				activTeacher.getDataBaseDisciplines().saveDB();
				activTeacher.getDataBaseDisciplines().updateDB();
				
				int c = tabsPanelModul.getTabCount();
				c--;
				tabsPanelModul.remove(c);
				
			}
		});

	}

	public void setCloseAction(ActionListener action) {
		cancel.addActionListener(action);

	}

	public String DateConvert(Date date) throws ParseException
	{   
	    
		String strDate = null;
		SimpleDateFormat smpFrtDt = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
		Date newDate = new SimpleDateFormat("EE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH).parse(date.toString());
		
		strDate = smpFrtDt.format(newDate);
		
		return strDate;
	}
	public void update(Teacher teacher) {
		super.update(teacher);
	}
}
