package ru.lms.modul;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.lms.client.tools.Group;
import ru.lms.client.tools.Lecture;
import ru.lms.client.tools.PracticalClasses;
import ru.lms.client.tools.Student;

import com.alee.laf.button.WebButton;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.table.WebTable;
/**
 * 
 * Модуль для отображения учета посещаемости студента практических занятий
 *
 */
@SuppressWarnings("serial")
public class ModulTablePractic extends Modul
{

	private static final Logger log = LoggerFactory.getLogger(ModulTablePractic.class);

	private WebPanel mainPanel;
	private WebTable table;
	private DefaultTableModel modelTable;
	private WebButton cancel;
	
	public ModulTablePractic(String nameModul)
	{
		this.nameModul = nameModul;
		this.mainPanel = new WebPanel();
		mainPanel.setBackground(new Color(255, 255, 255));

		this.init();
	}

	private void init()
	{
		log.info("Открыта таблица ведения учата посещаемости практических занятий");
		modelTable = new DefaultTableModel();
		table = new WebTable(modelTable);
		table.setColumnSelectionAllowed(true);

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(mainPanel, GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(mainPanel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
		);
		
		WebPanel panel_1 = new WebPanel();
		
		WebButton btnNewButton = new WebButton("Загрузить список студентов");
		
		cancel = new WebButton("Отмена");
		GroupLayout gl_panel = new GroupLayout(mainPanel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE)
						.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
							.addComponent(btnNewButton)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(cancel)))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(24)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(cancel)
						.addComponent(btnNewButton))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 231, Short.MAX_VALUE)
					.addContainerGap())
		);
			
		panel_1.setLayout(new BorderLayout(0, 0));

		mainPanel.setLayout(gl_panel);
		setLayout(groupLayout);
		mainPanel.setBackground(new Color(255, 255, 255));

		table.setColumnSelectionAllowed(true);

		//modelTable.setRowCount(row);

		panel_1.add(new WebScrollPane(table), BorderLayout.CENTER);

	

	}

	public void setCloseAction(ActionListener action)
	{
		cancel.addActionListener(action);
	}
	
	@Override
	public void update(Group group)
	{
		super.update(group);
		
		final DefaultTableModel tableModel = new DefaultTableModel();
		
		tableModel.addColumn(group.getNameGroup());
		
		for(PracticalClasses prac : group.getPracticalClassesList())
		{
			tableModel.addColumn(prac.getNamePractical());
		}
		
		for(Student stud : group.getStudentsList())
		{
			Vector<String> rowData = new Vector<String>();
			rowData.add(stud.getFirstName()+" "+stud.getSecondName());
			
			for(PracticalClasses prac : stud.getPracticalClassesList())
			{
				rowData.add(prac.getVisit());
			}
			
			tableModel.addRow(rowData);
		}
		
		this.table.setModel(tableModel);
		
		table.addMouseListener(new MouseListener() {
			
			 
			public void mousePressed(MouseEvent event) {
				if(event.getButton() == MouseEvent.BUTTON3) {
					Point point = event.getPoint();
					int col = table.columnAtPoint(point);
					int row = table.rowAtPoint(point);
					
					int collab =tableModel.getColumnCount();
					int rowlab =tableModel.getRowCount();
					
					if(col>=1 && row!=-1 && col<collab+1 && row<rowlab+1){
					tableModel.setValueAt("-", row, col);
					table.clearSelection();}
					else {
						table.clearSelection();
						}
					
				}
			}
			
			

			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		 
		              	  
		}); 
		
		
		table.addMouseListener(new MouseListener() {
			
			 
			public void mousePressed(MouseEvent event) {
				if(event.getButton() == MouseEvent.BUTTON1) {
					int col = table.getSelectedColumn();
					int row = table.getSelectedRow();
					
					int collab = tableModel.getColumnCount();
					int rowlab = tableModel.getRowCount();
					
					if(col>=1 && row!=-1 && col<collab+1 && row<rowlab+1){
					tableModel.setValueAt("+", row, col);
					table.clearSelection();}
					else {
						table.clearSelection();
						}
					
				}
			}

			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		 
		              	  
		});
	}
}
