package ru.lms.modul;

/**
 * Интерфейс, определяющий методы для добавления, 
 * удаления и оповещения модулей.
 */
public interface ObservableModul
{
	/**Добавляет модуль.*/
	void registerObserverModul(Modul modul);
	/**Удаляет модуль.*/
	void removeObserverModul(Modul modul);
	/**Оповещает о изменении преподователя всем модулям.*/
	void notifyTeacherModul();
	
}
