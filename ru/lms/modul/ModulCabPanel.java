package ru.lms.modul;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Window;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import com.alee.extended.date.WebCalendar;
import com.alee.extended.image.WebImage;
import com.alee.extended.painter.DashedBorderPainter;
import com.alee.laf.label.WebLabel;
import com.alee.laf.menu.WebMenu;
import com.alee.laf.menu.WebMenuBar;
import com.alee.laf.menu.WebMenuItem;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.text.WebTextField;

import ru.lms.client.teacher.FrameLoginTeacher;
import ru.lms.client.teacher.gui.TabsPanelModul;
import ru.lms.client.tools.Discipline;
import ru.lms.client.tools.Group;
import ru.lms.client.tools.Lecture;
import ru.lms.client.tools.Student;
import ru.lms.client.tools.Teacher;
import ru.lms.client.tools.database.xml.DataBaseXml;
import ru.lms.lang.Language;
import ru.lms.tools.Resources;

import javax.swing.UIManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPopupMenu;

import java.awt.Component;

import javax.swing.JPanel;

import java.awt.Insets;
import java.awt.SystemColor;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.swing.JLabel;

/**Модуль кабинет препода*/
@SuppressWarnings("serial")
public class ModulCabPanel extends Modul {


	
   @SuppressWarnings("unused")
   private static final Logger log = LoggerFactory.getLogger(ModulCabPanel.class);
   
	private WebPanel mainPanel;
	private WebTextField comboTextAddDis;
	private TabsPanelModul tabsPanelModul;
	private WebMenu dis;
	private WebMenuItem  create;
	private WebMenuItem addDis;
	private WebMenuBar menuBar; 
	private WebMenu chan;
	private WebMenuItem delGroup;
	private WebLabel LWelcom;
	private String input;
	private WebLabel today;
	private JPanel panel;

	

	public  ModulCabPanel()
	{   
		this(null);
	}
	
	public  ModulCabPanel(TabsPanelModul tabsPanelModul)
	{   		
		this.tabsPanelModul = tabsPanelModul;
		
		this.init();	
	}

	@SuppressWarnings("rawtypes")
	private void init()
	{
		log.info("Открыт кабинет");
		
		this.nameModul = Language.LABEL_CABINET;
		
		this.setLayout(new BorderLayout());
		
		this.mainPanel = new WebPanel();
		mainPanel.setForeground(Color.WHITE);
		//mainPanel.setBorder(UIManager.getBorder("ScrollPane.border")); // -- какая-то черная полоса
		mainPanel.setBackground(new Color(255, 255, 255));
		
		this.add(this.mainPanel,BorderLayout.CENTER);
		
        final DashedBorderPainter bp4 = new DashedBorderPainter ( new float[]{ 3f, 3f } ); //украшалка :3
        bp4.setRound ( 12 );
        bp4.setWidth ( 2 );
        bp4.setColor ( new Color ( 39, 95, 173 ) );
        
		comboTextAddDis = new WebTextField();
		comboTextAddDis.setColumns(10);
		String discipliPath = "DisciplineList.xml";
		new DataBaseXml(discipliPath);
		
		JPanel panelWel = new JPanel();
		panelWel.setBackground(Color.WHITE);
		
		WebPanel panelBar = new WebPanel(new BorderLayout());
		panelBar.setBackground(Color.WHITE);
		
		today = new WebLabel("Сегодня: ");
		today.setForeground(Color.DARK_GRAY);
		today.setFont(new Font("Sylfaen", Font.PLAIN, 27));
		
		WebImage webImage1 = new WebImage (Resources.MENU_ICON);
		
		panel = new JPanel();
		
		panel.setForeground(Color.WHITE);
		GroupLayout gl_mainPanel = new GroupLayout(mainPanel);
		gl_mainPanel.setHorizontalGroup(
			gl_mainPanel.createParallelGroup(Alignment.LEADING)
				.addComponent(panelBar, GroupLayout.DEFAULT_SIZE, 841, Short.MAX_VALUE)
				.addGroup(gl_mainPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(panelWel, GroupLayout.PREFERRED_SIZE, 619, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(212, Short.MAX_VALUE))
				.addGroup(gl_mainPanel.createSequentialGroup()
					.addGap(211)
					.addComponent(today, GroupLayout.PREFERRED_SIZE, 534, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(96, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_mainPanel.createSequentialGroup()
					.addContainerGap(598, Short.MAX_VALUE)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 290, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_mainPanel.setVerticalGroup(
			gl_mainPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_mainPanel.createSequentialGroup()
					.addComponent(panelBar, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
					.addGap(40)
					.addComponent(panelWel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(29)
					.addComponent(today, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE))
		);
		panel.setLayout(new BorderLayout(0, 0));
		panel.add(webImage1, BorderLayout.CENTER);
		//Добро пожаловать 
		LWelcom = new WebLabel(Language.LABEL_WELCOM_CABINET);
		LWelcom.setForeground(Color.DARK_GRAY);
		LWelcom.setHorizontalAlignment(SwingConstants.LEFT);
		LWelcom.setVerticalAlignment(SwingConstants.TOP);
		LWelcom.setFont(new Font("Sylfaen", Font.PLAIN, 35));
		panelWel.add(LWelcom);
		mainPanel.setLayout(gl_mainPanel);

		menuBar = new WebMenuBar();
		menuBar.setMargin(new Insets(1, 1, 1, 1));
		menuBar.setBorderColor(UIManager.getColor("Button.disabledForeground"));
		panelBar.add(menuBar, BorderLayout.CENTER);
		dis = new WebMenu("Дисциплины");
		chan = new WebMenu("Изменить личные данные");
		addDis = new WebMenuItem("Добавить дисциплину");
		WebMenu organ = new WebMenu("Органайзер");
		WebMenu other = new WebMenu("Разные разности :3");
		chan.setIcon(new ImageIcon(Resources.EDIT_PROF_MENU_ICON));
		WebMenu mes = new WebMenu("Разослать оповещение");
		mes.setIcon(new ImageIcon(Resources.NOTIFY_MENU_ICON));
		//create = new WebMenuItem("Создать новый учебный план");
		delGroup = new WebMenuItem("Удалить группу");

		dis.setDisabledFg(UIManager.getColor("Button.shadow"));
		dis.setBackground(SystemColor.activeCaptionBorder);
		dis.setHorizontalAlignment(SwingConstants.RIGHT);
		dis.setIcon(new ImageIcon(Resources.DIS_MENU_ICON));
		addDis.setIcon(new ImageIcon(Resources.LOGIN_TEACHER_ICON));
		organ.setIcon(new ImageIcon(Resources.ORGANAIZER_MENU_ICON));
		other.setIcon(new ImageIcon(Resources.OTHER_MENU_ICON));
		
		delGroup.setIcon(new ImageIcon(Resources.DEL_CURR_MENU_ICON));

	
		
		dis.add(addDis);
		dis.addSeparator();
		
		menuBar.add(dis);
		menuBar.add(organ);
		menuBar.add(chan);
		menuBar.add(mes);
		menuBar.add(other);
		
		
		addDis.addActionListener(new ActionListener()
		{
			
			
			
			public void actionPerformed(ActionEvent e)
			{
				Window win = null;
				
			    input = WebOptionPane.showInputDialog( win, "Наименование дисциплины", "Добавление новой дисциплины", WebOptionPane.QUESTION_MESSAGE, null, null,"" ).toString();
				System.out.println(input);
				
				
				if (input == null || input.equals(""))
					return;
				Discipline newDis = new Discipline(input);
				
				newDis=activTeacher.getDataBaseDisciplines().addDisciplineToDB(newDis);
				newDis.updateDB();
				newDis.getDataBase().saveDB();
				
				activTeacher.getDisciplineList().add(newDis);
				update();
				
				
			
				
			}
		 
		});
		
	}

	@Override
	public void update(final Teacher teacher)
	{
		this.activTeacher = teacher;
		
		for(int i=2; i<dis.getComponentCount(); i++)
		{
			dis.remove(i);
		}
		
		if (this.activTeacher != null)
		{
			this.LWelcom.setText(Language.LABEL_WELCOM_CABINET + " " + 
		this.activTeacher.getFirstName() + " " +
		this.activTeacher.getSecondName());
			
			String date = new SimpleDateFormat ( "EEEE" ).format ( new Date() );
			String str = null;
			try {
				str = DateConvert(new java.util.Date ());
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			this.today.setText("Сегодня:  "+ date + "  "+str); 
			
		   
			
			for (final Discipline disc : this.activTeacher.getDisciplineList())
			{   
				
				
				WebMenu disMenu = new WebMenu(disc.getName());
				final WebMenuItem create = new WebMenuItem("Создать новый учебный план");
				create.setIcon(new ImageIcon(Resources.ADD_CURR_MENU_ICON));
				disMenu.add(create);
				
			
				disMenu.setIcon(new ImageIcon(Resources.DISCIPLINE_MENU_ICON));
				
				create.addActionListener(new ActionListener()
				{
					
					
					public void actionPerformed(ActionEvent e)
					{
					
						ModulAddCurrPanel cab = new ModulAddCurrPanel(tabsPanelModul, "ТОИ");
						
						// Добавляю в табпанель кабинет
						tabsPanelModul.registerObserverModul(cab);
						cab.update(activTeacher);
						
						
					}
				});
				
				
			
				
				for(final Group group : disc.getGroupList())
				{
					WebMenu groupMenu = new WebMenu(group.getNameGroup());
					groupMenu.setIcon(new ImageIcon(Resources.GROUP_MENU_ICON));
					
					WebMenuItem lec = new WebMenuItem("Лекции");
					
					WebMenuItem prac = new WebMenuItem("Практические занятия");
					
					WebMenuItem total = new WebMenuItem("Лабораторные, тесты");
					
					
					lec.setIcon(new ImageIcon(Resources.CLASSES_MENU_ICON));
					prac.setIcon(new ImageIcon(Resources.CLASSES_MENU_ICON));
					total.setIcon(new ImageIcon(Resources.CLASSES_MENU_ICON));
					
					
					lec.addActionListener(new ActionListener()
					{
						
						@Override
						public void actionPerformed(ActionEvent e)
						{
							final ModulTableLec lecModul = new ModulTableLec("Лекции группы " +group.getNameGroup());
							lecModul.update(group);
							lecModul.setCloseAction(new ActionListener()
							{
								
								@Override
								public void actionPerformed(ActionEvent e)
								{
									ModulCabPanel.this.tabsPanelModul.removeObserverModul(lecModul);
								}
							});
							ModulCabPanel.this.tabsPanelModul.registerObserverModul(lecModul);
						}
					});
					
					prac.addActionListener(new ActionListener()
					{
						
						@Override
						public void actionPerformed(ActionEvent e)
						{
							final ModulTablePractic pracModul = new ModulTablePractic("Практические занятия группы " +group.getNameGroup());
							pracModul.update(group);
							pracModul.setCloseAction(new ActionListener()
							{
								
								@Override
								public void actionPerformed(ActionEvent e)
								{
									ModulCabPanel.this.tabsPanelModul.removeObserverModul(pracModul);
								}
							});
							ModulCabPanel.this.tabsPanelModul.registerObserverModul(pracModul);
						}
					});
					
					total.addActionListener(new ActionListener()
					{
						
						@Override
						public void actionPerformed(ActionEvent e)
						{
							final ModulLabTestTotaly totModul = new ModulLabTestTotaly("Лабораторные, тесты группы " +group.getNameGroup());
							totModul.update(group);
							totModul.setCloseAction(new ActionListener()
							{
								
								@Override
								public void actionPerformed(ActionEvent e)
								{
									ModulCabPanel.this.tabsPanelModul.removeObserverModul(totModul);
								}
							});
							ModulCabPanel.this.tabsPanelModul.registerObserverModul(totModul);
						}
					});
					
					
					groupMenu.add(lec);
					groupMenu.add(prac);
					groupMenu.add(total);
					disMenu.add(groupMenu);
					
				}
				
				dis.add(disMenu);
			
				
			}
		}
		
		
	}


	@SuppressWarnings("deprecation")
	public void update()
	
	{
		
	
		
		if (this.activTeacher != null)
		{
			
			dis.removeAll();
			
			dis.add(addDis);
			dis.addSeparator();
			
			this.LWelcom.setText(Language.LABEL_WELCOM_CABINET + " " + 
		this.activTeacher.getFirstName() + " " +
		this.activTeacher.getSecondName());
			
			

			
			
			for (final Discipline disc : this.activTeacher.getDisciplineList())
			{   
				
				
				WebMenu disMenu = new WebMenu(disc.getName());
				final WebMenuItem create = new WebMenuItem("Создать новый учебный план");
				create.setIcon(new ImageIcon(Resources.ADD_CURR_MENU_ICON));
				disMenu.add(create);
				
			
				disMenu.setIcon(new ImageIcon(Resources.DISCIPLINE_MENU_ICON));
				
				create.addActionListener(new ActionListener()
				{
					
					
					public void actionPerformed(ActionEvent e)
					{
					
						ModulAddCurrPanel cab = new ModulAddCurrPanel(tabsPanelModul, "ТОИ");
						
						// Добавляю в табпанель кабинет
						tabsPanelModul.registerObserverModul(cab);
						
						
					}
				});
				
				
			
				
				for(final Group group : disc.getGroupList())
				{
					WebMenu groupMenu = new WebMenu(group.getNameGroup());
					groupMenu.setIcon(new ImageIcon(Resources.GROUP_MENU_ICON));
					
					WebMenuItem lec = new WebMenuItem("Лекции");
					
					WebMenuItem prac = new WebMenuItem("Практические занятия");
					
					WebMenuItem total = new WebMenuItem("Лабораторные, тесты");
					
					
					lec.setIcon(new ImageIcon(Resources.CLASSES_MENU_ICON));
					prac.setIcon(new ImageIcon(Resources.CLASSES_MENU_ICON));
					total.setIcon(new ImageIcon(Resources.CLASSES_MENU_ICON));
					
					
					lec.addActionListener(new ActionListener()
					{
						
						@Override
						public void actionPerformed(ActionEvent e)
						{
							final ModulTableLec lecModul = new ModulTableLec("Лекции группы " +group.getNameGroup());
							lecModul.update(group);
							lecModul.setCloseAction(new ActionListener()
							{
								
								@Override
								public void actionPerformed(ActionEvent e)
								{
									ModulCabPanel.this.tabsPanelModul.removeObserverModul(lecModul);
								}
							});
							ModulCabPanel.this.tabsPanelModul.registerObserverModul(lecModul);
						}
					});
					
					prac.addActionListener(new ActionListener()
					{
						
						@Override
						public void actionPerformed(ActionEvent e)
						{
							final ModulTablePractic pracModul = new ModulTablePractic("Практические занятия группы " +group.getNameGroup());
							pracModul.update(group);
							pracModul.setCloseAction(new ActionListener()
							{
								
								@Override
								public void actionPerformed(ActionEvent e)
								{
									ModulCabPanel.this.tabsPanelModul.removeObserverModul(pracModul);
								}
							});
							ModulCabPanel.this.tabsPanelModul.registerObserverModul(pracModul);
						}
					});
					
					total.addActionListener(new ActionListener()
					{
						
						@Override
						public void actionPerformed(ActionEvent e)
						{
							final ModulLabTestTotaly totModul = new ModulLabTestTotaly("Лабораторные, тесты группы " +group.getNameGroup());
							totModul.update(group);
							totModul.setCloseAction(new ActionListener()
							{
								
								@Override
								public void actionPerformed(ActionEvent e)
								{
									ModulCabPanel.this.tabsPanelModul.removeObserverModul(totModul);
								}
							});
							ModulCabPanel.this.tabsPanelModul.registerObserverModul(totModul);
						}
					});
					
					
					groupMenu.add(lec);
					groupMenu.add(prac);
					groupMenu.add(total);
					disMenu.add(groupMenu);
					
				}
				
				dis.add(disMenu);
			
			}
			
		}
		
	}
	public String DateConvert(Date date) throws ParseException
	{   
	    
		String strDate = null;
		SimpleDateFormat smpFrtDt = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
		Date newDate = new SimpleDateFormat("EE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH).parse(date.toString());
		
		strDate = smpFrtDt.format(newDate);
		
		return strDate;
	}
}
