package ru.lms.modul;

import java.text.ParseException;

import com.alee.laf.panel.WebPanel;

import ru.lms.client.tools.Discipline;
import ru.lms.client.tools.Group;
import ru.lms.client.tools.Teacher;

/**
 * Абстрактный класс для модулей.
 */
@SuppressWarnings("serial")
public abstract class Modul extends WebPanel implements ModulInt, ObserverModul
{
	/**Название модуля.*/
	protected String nameModul;
	
	protected Teacher activTeacher;
	
	public Modul()
	{
		this.nameModul = "Modul";
	}
	
	@Override
	public void update(Teacher teacher)
	{
		this.activTeacher = teacher;
	}
	
	@Override
	public void update(Discipline discipline)
	{

	}
	
	@Override
	public void update(Group group)
	{

	}
	
	@Override
	public void update()
	{

	}

	public String getNameModul()
	{
		return nameModul;
	}

	public void setNameModul(String nameModul)
	{
		this.nameModul = nameModul;
	}
	
	
}
