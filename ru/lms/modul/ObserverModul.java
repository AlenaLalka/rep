package ru.lms.modul;

import ru.lms.client.tools.Discipline;
import ru.lms.client.tools.Group;
import ru.lms.client.tools.Teacher;

/**
 * Интерфейс, с помощью которого наблюдатель получает оповещение
 */
public interface ObserverModul
{
	/**
	 * Получает активного пользователя преподаватель.
	 */
	void update(Teacher teacher);
	
	void update(Discipline discipline);
	
	void update(Group group);
}
