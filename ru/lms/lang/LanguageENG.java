package ru.lms.lang;

/**
 * Язык по-умолчанию английский.
 */
public class LanguageENG extends Language
{
	public LanguageENG()
	{
		Language.LABEL_LOGIN = "Login";
		LanguageENG.LABEL_PASSWORD = "Password";
	}
}
