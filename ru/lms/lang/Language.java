package ru.lms.lang;

/**
 * Языковые константы. Стандартный русский язык.
 */
public class Language
{
	public static String TITLE_TEACHER_FRAME = "LMS";
	//===============LABELS=====================
	public static String LABEL_TEACHER_CLIENT_FRAME = "Няшный преподаватель";
	public static String LABEL_ADD_CURR="Добавление учебного плана";
	public static String LABEL_TEACHER_LOGIN_FRAME = "Вход в киентскую часть преподователя";
	public static String LABEL_TEACHER_REGISTER_FRAME = "Зарегистрировать нового преподователя";
	public static String LABEL_CONFIRM_PASSWORD = "Подтверждение пароля";
	public static String LABEL_LOGIN = "Введите логин";
	public static String LABEL_PASSWORD = "Введите пароль";
	public static String LABEL_FIRST_NAME = "Имя";
	public static String LABEL_SECOND_NAME = "Фамилия";
	public static String LABEL_EMAIL = "E-Mail";
	public static String LABEL_WORKSPACE_PATH = "Папка рабочего пространства";
	public static String LABEL_WELCOM_CABINET = "Добро пожаловать";
	public static String LABEL_CABINET = "Кабинет";
	public static String LABEL_FILE = "Файл";
	public static String LABEL_HELP= "Справка";
	public static String LABEL_ABOUT_PROGRAM= "О программе";
	public static String LABEL_EXIT= "Выход";
	public static String LABEL_OPEN= "Открыть";
	
	public static String LABEL_TOTAL_POINT="Итого за семестр баллов: ";
	public static String LABEL_LABORATORY ="Лабораторная: ";
	public static String LABEL_NAME_LABORATORY ="Название лабораторной: ";
	public static String LABEL_POINT_OFLAB = "Баллы за лабораторную: ";
	public static String LABEL_TEST = "Тест: ";
	public static String LABEL_NAME_TEST = "Название теста: ";
	public static String LABEL_POINT_OFTEST = "Баллы за тест: ";
	
	public static String LABEL_COUNT_LEC_CURR = "Количество лекций: ";
	public static String LABEL_ENTER_PRAC_CURR = "Количество практических занятий: ";
	public static String LABEL_ENTER_LAB_CURR = "Количество лабораторных работ: ";
	public static String LABEL_ENTER_TEST_CURR= "Количество тестов: ";
	public static String LABEL_ENTER_COUNT_STUD= "Количество студентов в группе: ";
	public static String LABEL_ENTER_NAME_GROUP= "Имя группы: ";
	public static String LABEL_TITEL_ADD_CURR= "Создание учебного плана на семестр";
	public static String LABEL_POINT_CURR= "Балл за посещение: ";
	//===============BUTTONS=====================
	public static String BUTTTON_ENTER = "Войти";
	public static String BUTTTON_REGISTER = "Зарегистрироваться";
	public static String BUTTTON_CANCEL = "Отмена";
	public static String BUTTTON_ADD_DISCIPLINE = "Добавить дисциплину";
	public static String BUTTTON_CHANGE_DATA_TEACHER = "Изменение личных данных";
	public static String BUTTTON_ORGANIZER = "Органайзер";
	public static String BUTTTON_NEXT = "Далее";
	public static String BUTTTON_SAVE = "Сохранить";
	
	
	
}
