package ru.lms.tools;

import java.net.URL;

/**
 * Ресурсы проекта (картинты, пути и т.д.)
 */
public interface Resources
{
	/**Путь к списку зарегистрированных преподавателей.*/
	public static final String PATH_TEACHER_LIST = "DataBase/UserList/Teacher";
	public static final String PATH_WORKSPACE = "DataBase/";
	
	/**Иконка окна для аутификации преподавателя.*/
	public static final URL LOGIN_TEACHER_ICON = Resources.class.getResource("/icon.png");
	
	public static final URL BACKGROUND_IMAGE = Resources.class.getResource("/img.png");
	
	public static final URL DIS_MENU_ICON = Resources.class.getResource("/book_bookmarks_5248.png");
	
	public static final URL DIS_MENU_ITEM_ICON = Resources.class.getResource("/folder-tar_7152.png");
	
	public static final URL EDIT_PROF_MENU_ICON = Resources.class.getResource("/pencil3_7221.png");
	
	public static final URL NOTIFY_MENU_ICON = Resources.class.getResource("/email_2346.png");
	
	public static final URL ORGANAIZER_MENU_ICON = Resources.class.getResource("/org.png");
	
	public static final URL ADD_CURR_MENU_ICON = Resources.class.getResource("/docNew.png");
	
	public static final URL DEL_CURR_MENU_ICON = Resources.class.getResource("/delgroup.png");
	
	public static final URL OTHER_MENU_ICON = Resources.class.getResource("/cup.png");
	
	public static final URL DISCIPLINE_MENU_ICON = Resources.class.getResource("/disc.png");
	
	public static final URL CLASSES_MENU_ICON = Resources.class.getResource("/doc.png");
	
	public static final URL GROUP_MENU_ICON = Resources.class.getResource("/group.png");
	
	public static final URL MENU_ICON = Resources.class.getResource("/mainImg.png");

}
