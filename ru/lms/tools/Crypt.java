package ru.lms.tools;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Хеш-методы.
 */
public class Crypt
{
	/**
	 * Расчет хэш-суммы у строки.
	 * 
	 * @param строка
	 *            .
	 * @return хэш-суммму в виде String.
	 */
	public static String getStringToMD5(String str)
	{
		String hash = null;

		if (str == null)
			return "";

		try
		{
			MessageDigest m = MessageDigest.getInstance("MD5");
			m.update(str.getBytes(), 0, str.length());
			hash = new BigInteger(1, m.digest()).toString(16);
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return hash;
	}

	/**
	 * Расчет хэш-суммы у файла.
	 * 
	 * @param path
	 *            Путь к файлу.
	 * @return хэш-суммму в виде String.
	 */
	public static String getFileToMD5(String path)
	{
		String checksum = null;
		try
		{
			@SuppressWarnings("resource")
			FileInputStream fis = new FileInputStream(path);
			MessageDigest md = MessageDigest.getInstance("MD5");

			byte[] buffer = new byte[8192];
			int numOfBytesRead;
			while ((numOfBytesRead = fis.read(buffer)) > 0)
			{
				md.update(buffer, 0, numOfBytesRead);
			}
			byte[] hash = md.digest();
			checksum = new BigInteger(1, hash).toString(16);
		} catch (IOException ex)
		{
			ex.printStackTrace();
		} catch (NoSuchAlgorithmException ex)
		{
		}

		return checksum;
	}
}
